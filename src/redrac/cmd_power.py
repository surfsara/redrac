#
#   redrac cmd_power.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: power

Controls the power of the system; power on/off and such
'''

import argparse

from typing import List, Tuple

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.hostconf import get_host
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_power(host: str, args: argparse.Namespace) -> None:
    '''handle power command

    power : show current power state
    power --list : list available power states
    power -f state : apply new power state
    '''

    if args.list:
        # only list available (supported) power states
        show_power_actions(host, args)

    elif args.force:
        # power on/off system
        apply_powerstate(host, args, args.force)

    else:
        # show current powerstate of the system
        show_powerstate(host, args)


def show_power_actions(host: str, args: argparse.Namespace) -> None:
    '''power --list : show available power commands

    Get the Redfish ResetType(s) from the remote controller
    and show them
    '''

    reset_types = load_reset_types(host, args)
    for reset_type in sorted(reset_types):
        print(f'  {reset_type}')


def load_reset_types(host: str, args: argparse.Namespace) -> List[str]:
    '''Returns list of ResetType(s) that the remote controller supports

    May raise RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        _reset_url, reset_types = obtain_reset_info(redfish_obj)
        return reset_types


def obtain_reset_info(redfish_obj: RedfishClient) -> Tuple[str, List[str]]:
    '''get information on how to reset this system
    Returns tuple: (reset target URL, list of ResetTypes)

    May raise RedfishError if not found
    '''

    reset_url = None
    reset_types = None

    systems_members = redfish_obj.load('Systems:Members')
    for data in systems_members:
        if reset_types is None:
            try:
                reset_types = data['Actions']['#ComputerSystem.Reset']['ResetType@Redfish.AllowableValues']
                debug(f'ResetType@Redfish.AllowableValues == {reset_types!r}')
            except KeyError:
                try:
                    actioninfo_url = data['Actions']['#ComputerSystem.Reset']['@Redfish.ActionInfo']
                    actioninfo = redfish_obj.get(actioninfo_url)
                    for param in actioninfo['Parameters']:
                        try:
                            if param['Name'] == 'ResetType':
                                reset_types = param['AllowableValues']
                                debug(f'ResetType AllowableValues == {reset_types!r}')
                        except KeyError:
                            pass
                except KeyError:
                    pass

        if reset_url is None:
            try:
                reset_url = data['Actions']['#ComputerSystem.Reset']['target']
                debug(f'reset_url == {reset_url!r}')
            except KeyError:
                pass

    if reset_url is None:
        raise RedfishError('ComputerSystem.Reset target URL not found')

    if reset_types is None:
        raise RedfishError('ResetTypes not found')

    return reset_url, reset_types


def show_powerstate(host: str, args: argparse.Namespace) -> None:
    '''show the power state of the system'''

    powerstate = load_powerstate(host, args)
    print(f'power: {powerstate}')


def load_powerstate(host: str, args: argparse.Namespace) -> str:
    '''get powerstate from remote controller using redfish
    Returns powerstate as string
    May raise RedfishError
    '''

    powerstate = 'unknown'

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Systems:Members')
        for data in data_list:
            try:
                powerstate = data['PowerState']
                break               # return first found powerstate
            except KeyError:
                continue

    debug(f'powerstate == {powerstate!r}')
    return powerstate


def apply_powerstate(host: str, args: argparse.Namespace, state: str) -> None:
    '''give a generic power command'''

    host, username, password, cafile = get_host(host, args).as_tuple()
    redfish_system_reset(host, username, password, cafile, state)
    print(f'{host}: ok')


def redfish_system_reset(host: str, username: str, password: str, cafile: str, state: str) -> None:
    '''reset the system
    state is the ResetType (may be in lowercase)
    like: 'on', 'forceoff', 'powercycle', etc.

    May raise RedfishError
    '''

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        reset_url, available_reset_types = obtain_reset_info(redfish_obj)

        # we may provide the reset type in lowercase
        # when we POST the ResetType, it must be spelled as it appears in the available ResetTypes
        # (which is usually upper camelcase)

        lower_reset_types = [x.lower() for x in available_reset_types]
        if state.lower() not in lower_reset_types:
            raise ValueError(f'reset type not available: {state!r}')

        # get the ResetType in correct spelling  (upper camelcase)
        reset_types_map = {x.lower(): x for x in available_reset_types}
        post_reset_type = reset_types_map[state.lower()]

        # reset the system!
        post_body = {'ResetType': post_reset_type}
        redfish_obj.post(reset_url, body=post_body)


# EOB
