#
#   redrac cmd_path.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: path

Get Redfish URL(s) for given pseudo-path
or import/export Redfish object via pseudo-path
'''

import argparse
import json

from typing import List

from redrac import progress
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.print import print
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import path_match_urls, url_match_urls
from redrac.cmd_url import load_import_file, load_json_file, do_url_http_request
from redrac.util import print_data


def do_path(host: str, args: argparse.Namespace) -> None:
    '''get/export/import Redfish object via pseudo-path
    or print the URL(s) for given pseudo-path
    '''

    # NOTE 'path' is much the same as the 'url' command;
    # in the end 'path' calls the 'url' code

    do_request = args.get or args.post or args.patch or args.delete
    if not do_request:
        if args.import_file:
            # if importing a file, default is a PATCH request
            args.patch = True
        else:
            # default is a GET request
            args.get = True

    if args.export:
        args.json = True

    # by default, 'path' shows the URL(s)
    # sometimes it makes more sense to directly do an action
    # (like was also the case in older `redrac`)
    if not do_request and (args.export or args.import_file or args.template):
        # do a default action
        do_request = True

    host, username, password, cafile = get_host(host, args).as_tuple()

    # load body and headers (the same as in `do_url()`)
    body = {}
    headers = {}
    if args.import_file:
        # --import is a backwards compatible option; same as --body
        body = load_import_file(args.import_file)

    if args.body_file:
        body = load_import_file(args.body_file)

    if args.headers_file:
        headers = load_json_file(args.headers_file)

    if body and not headers:
        # set default headers
        headers = {'If-Match': '*',
                   'Content-Type': 'application/json'}

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            urls = redfish_obj.query_urls(args.path)
            debug(f'urls == {urls!r}')
        except (KeyError, IndexError) as err:
            raise RedfishError('invalid path; Redfish URL not found') from err

        if not urls:
            raise RedfishError('invalid path; Redfish URL not found')

        if args.match:
            # select URL by path-match pattern
            urls = [path_match_urls(redfish_obj, args.match, urls), ]
        if args.url_match:
            # select URL by url-match pattern
            urls = [url_match_urls(args.url_match, urls), ]

        if not do_request:
            # show matching URLs
            if args.json:
                print(json.dumps(urls))
            else:
                for url in urls:
                    print(url)
            return

        if len(urls) > 1:
            if args.get and not args.import_file and not args.template:
                show_multiple_urls(redfish_obj, urls, args)
                return

            debug('multiple URLs; using the first one')
        try:
            args.url = urls[0]
        except IndexError as err:
            raise RedfishError('invalid path; Redfish URL not found') from err

        # execute same as the 'url' command
        do_url_http_request(redfish_obj, args, body, headers)


def show_multiple_urls(redfish_obj: RedfishClient, urls: List[str], args: argparse.Namespace) -> None:
    '''show the Redfish objects for multiple URLs'''

    data = {'redrac': True,
            'path': args.path,
            'objects': []}

    for url in urls:
        obj = {'url': url, 'data': redfish_obj.get(url)}
        data['objects'].append(obj)

    print_data(data, as_json=args.json)


# EOB
