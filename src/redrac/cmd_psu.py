#
#   redrac cmd_psu.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: psu

Shows information about the power supplies
'''

import argparse

from typing import List, Dict, Any

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient
from redrac.hostconf import get_host
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class PSU:
    '''represents a power supply'''

    def __init__(self, name: str, model: str, health: str, hotplug: bool = False, redundancy_type: str = '', capacity_watts: int = 0, input_power_watts: float = -1.0) -> None:
        '''initialize instance'''

        self.name = name
        self.model = model
        self.health = health
        self.hotplug = hotplug
        self.redundancy_type = redundancy_type
        self.capacity_watts = capacity_watts
        self.input_power_watts = input_power_watts

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'PSU':
        '''instantiate from data dictionary'''

        try:
            name = data['Name']
        except KeyError:
            name = '(psu)'

        try:
            model = data['Model']
        except KeyError:
            model = ''

        health = 'unknown'
        try:
            health = data['Status']['Health']

            # if the PSU is not enabled, then the health can not be 'OK' AFAIC
            if health.lower() == 'ok' and data['Status']['State'] != 'Enabled':
                health = 'Warning'

            if data['Status']['State'] == 'Absent':
                health = '(absent)'

        except KeyError:
            try:
                if data['Status']['State'] == 'Absent':
                    health = '(absent)'
            except KeyError:
                pass

        try:
            hotplug = data['HotPluggable']
        except KeyError:
            hotplug = False

        try:
            redundancy_type = data['RedundancyType']
        except KeyError:
            redundancy_type = ''

        try:
            capacity_watts = int(data['PowerCapacityWatts'])
        except (KeyError, ValueError):
            capacity_watts = 0

        try:
            input_power_watts = float(data['Metrics']['InputPowerWatts']['Reading'])
        except (KeyError, ValueError):
            input_power_watts = -1.0

        return PSU(name, model, health, hotplug, redundancy_type, capacity_watts, input_power_watts)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Name': self.name,
                'Model': self.model,
                'Health': self.health,
                'HotPluggable': self.hotplug,
                'RedundancyType': self.redundancy_type,
                'PowerCapacityWatts': self.capacity_watts,
                'InputPowerWatts': self.input_power_watts}

    def __repr__(self) -> str:
        '''Returns string'''

        return (f'PSU(name={self.name!r}, model={self.model!r}, health={self.health!r}, hotplug={self.hotplug!r}, '
                f'redundancy_type={self.redundancy_type!r}, capacity_watts={self.capacity_watts}, '
                f'input_power_watts={self.input_power_watts:.1f})')



def do_psu(host: str, args: argparse.Namespace) -> None:
    '''show PSU information'''

    data_list = load_psu(host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    psu_list = [PSU.from_data(x) for x in data_list]
    if not psu_list:
        print('power supplies not found')
        return

    width = max(len(x.name) for x in psu_list)
    model_width = max(len(x.model) for x in psu_list)
    health_width = max(len(x.health) for x in psu_list)

    for psu in psu_list:
        if psu.hotplug:
            hotplug = 'hotplug'
        else:
            hotplug = ''

        watts = ''
        if psu.input_power_watts > -1.0:
            watts += f'{psu.input_power_watts:>6.1f}W'
            if psu.capacity_watts > 0:
                watts += '/'
        if psu.capacity_watts > 0:
            watts += f'{psu.capacity_watts:>4}W'

        print(f'{psu.name:<{width}}  {psu.model:<{model_width}}  health: {psu.health:<{health_width}}  {hotplug:<7}  {psu.redundancy_type}  {watts}')


def load_psu(host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''get PSU data from remote controller

    Returns list of Redfish objects
    May raise RedfishError
    '''

    # a system may have multiple chassis members, where one chassis member
    # has redundant PSUs, while another chassis member has non-redundant PSUs
    # (however unlikely)
    # Also note, if there are multiple distinct redundancy groups, then we lose
    # that information because we simply flatten the PSU list

    data_list = []

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            data_list = redfish_obj.load('Chassis:Members:PowerSubsystem:PowerSupplyRedundancy:RedundancyGroup')
            if not data_list:
                raise KeyError('we have PSU redudancy, but none are present')
            debug('we have redundant PSUs')
        except KeyError:
            data_list = redfish_obj.load('Chassis:Members:PowerSubsystem:PowerSupplies:Members')
            if not data_list:
                debug('no power supplies found')
            else:
                debug('we have non-redundant PSUs')

        # try loading the metrics per PSU as well
        for data in data_list:
            try:
                url = data['Metrics']['@odata.id']
                data['Metrics'] = redfish_obj.get(url)
            except KeyError:
                pass

    return data_list


# EOB
