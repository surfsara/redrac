#
#   redrac hostconf.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''host configuration file code

The format of the config file is like:


    # /etc/redrac.conf

    # defaults entry "Host *" applies to all
    Host *
        User root

    Host *.mgmt.surf.nl
        Password secret123

    Host mgmt2
        IPaddress 192.168.0.10
        User admin
        Password idunnowhat
        CAfile /etc/ssl/certs/ca-bundle.pem

    # EOB


The main usage of this module is:

    host, username, password, cafile = get_host(host, args).as_tuple()

'''

import argparse
import fnmatch
import getpass
import socket
import copy
import re

from typing import List, Dict, Tuple, Optional

from redrac.debug import debug


class Host:
    '''groups together host, username, password, cafile'''

    def __init__(self, host: str = '', username: str = '', password: str = '', cafile: str = '') -> None:
        '''initialize instance'''

        self.host = host
        self.username = username
        self.password = password
        self.cafile = cafile

    def __repr__(self) -> str:
        '''Returns string'''

        return f'Host(host={self.host!r}, username={self.username!r}, password=xxxxxx, cafile={self.cafile!r})'

    def as_tuple(self) -> Tuple[str, str, str, str]:
        '''Returns tuple: (host, username, password, cafile)'''

        return self.host, self.username, self.password, self.cafile

    def apply_defaults_from(self, other: 'Host'):
        '''copy members from other, if they are currently not set'''

        if other.host and not self.host:
            self.host = other.host
        if other.username and not self.username:
            self.username = other.username
        if other.password and not self.password:
            self.password = other.password
        if other.cafile and self.cafile:
            self.cafile = other.cafile



class HostConf:
    '''represents a Host entry in a config file'''

    def __init__(self, filename: str, lineno: int, ident: str) -> None:
        '''initialize instance'''

        self.filename = filename
        self.lineno = lineno
        self.ident = ident
        self.host = Host()

    def __repr__(self) -> str:
        '''Returns string'''

        return f'HostConf(filename={self.filename!r}, lineno={self.lineno}, ident={self.ident!r}, host={self.host!r})'



class ConfigErrors(ValueError):
    '''exception for multiple config errors'''

    def __init__(self, placeholder: str, errors: List[Tuple[str, int, str]]) -> None:
        '''initialize instance

        `errors` is a list of tuples: [(filename, lineno, message), ]
        '''

        super().__init__(placeholder)
        self.errors = errors

    def __str__(self) -> str:
        '''Returns string'''

        messages = []
        for filename, lineno, errmsg in self.errors:
            if filename:
                msg = filename + ':'
            else:
                msg = ''
            if lineno > 0:
                msg += f'{lineno}:'
            if msg:
                msg += ' '
            msg += errmsg
            messages.append(msg)
        return '\n'.join(messages)



def load_config(filename: str = '') -> Dict[str, HostConf]:
    '''load config file
    Returns dict with HostConf entries by their identifier
    The identifier may be a wildcarded hostname

    May raise OSError
    Exits the program on any syntax errors in the config file
    '''

    if not filename:
        filename = '/etc/redrac.conf'
        use_default_config = True
    else:
        use_default_config = False

    try:
        return do_load_config(filename)
    except FileNotFoundError:
        # permit default config to be missing
        if use_default_config:
            return {}
        # else a specific config file was given;
        # re-raise the error
        raise


def do_load_config(filename: str) -> Dict[str, HostConf]:
    '''load config file
    Returns dict with HostConf entries by their identifier
    The identifier may be a wildcarded hostname

    May raise OSError, ConfigErrors
    '''

    hostmap = {}
    errors = []

    host_entry: Optional[HostConf] = None

    with open(filename, encoding='utf-8') as fh:
        lineno = 0
        while True:
            line = fh.readline()
            if not line:
                break

            lineno += 1

            line = line.strip()
            if not line:
                continue

            if line[0] == '#':
                # skip comment
                continue

            try:
                key, value = line.split(maxsplit=1)
            except ValueError:
                errors.append((filename, lineno, 'syntax error: expected a key/value pair'))
                continue

            if key.lower() not in ('host', 'user', 'password', 'cafile', 'ipaddress'):
                errors.append((filename, lineno, f'unknown keyword {key!r}'))
                continue

            if not value:
                errors.append((filename, lineno, 'syntax error: expected a value'))
                continue

            if key.lower() == 'host':
                if host_entry is not None:
                    # save host_entry in hostmap
                    hostmap[host_entry.ident] = host_entry
                    host_entry = None

                ident = value
                try:
                    entry = hostmap[ident]
                    errors.append((filename, lineno, f'redefinition of Host {ident}, defined on line {entry.lineno}'))
                    continue
                except KeyError:
                    # this is the good case
                    # entry does not exist yet in hostmap
                    # make a new host_entry
                    host_entry = HostConf(filename, lineno, ident)

            elif key.lower() == 'user':
                if host_entry is None:
                    errors.append((filename, lineno, 'syntax error; UserName must appear under a Host'))
                    continue
                if host_entry.host.username:
                    errors.append((filename, lineno, 'redefinition of UserName'))
                    continue
                host_entry.host.username = value

            elif key.lower() == 'password':
                if host_entry is None:
                    errors.append((filename, lineno, 'syntax error; Password must appear under a Host'))
                    continue
                if host_entry.host.password:
                    errors.append((filename, lineno, 'redefinition of Password'))
                    continue
                host_entry.host.password = value

            elif key.lower() == 'cafile':
                if host_entry is None:
                    errors.append((filename, lineno, 'syntax error; CAfile must appear under a Host'))
                    continue
                if host_entry.host.cafile:
                    errors.append((filename, lineno, 'redefinition of CAfile'))
                    continue
                host_entry.host.cafile = value

            elif key.lower() == 'ipaddress':
                if host_entry is None:
                    errors.append((filename, lineno, 'syntax error; IPaddress must appear under a Host'))
                    continue
                if host_entry.host.host:
                    errors.append((filename, lineno, 'redefinition of IPaddress'))
                    continue
                host_entry.host.host = value

            else:
                # this is a program bug, really
                # because we already checked the keywords above
                raise RuntimeError(f'unknown keyword {key!r}')

    if errors:
        raise ConfigErrors('there were config errors', errors)

    # save the final host_entry
    if host_entry is not None:
        hostmap[host_entry.ident] = host_entry

    # fixup of entry.host for all hostmap entries
    # this fills in the host identifier as host address, if 'IPaddress' was not used
    hostmap = {key: fixup_hostmap_entry(key, value) for key, value in hostmap.items()}

    debug(f'hostmap == {hostmap!r}')
    return hostmap


def fixup_hostmap_entry(ident: str, entry: HostConf) -> HostConf:
    '''put ident as hostname (where appropriate)
    Returns updated Host instance
    '''

    if not entry.host.host and not is_pattern(ident):
        # if no wildcards in ident then we set host address to the ident
        entry.host.host = ident
    #else
    # entry.host.host was already set by 'IPaddress' keyword
    return entry


def is_pattern(arg: str) -> bool:
    '''Returns true if string contains wildcard symbols'''

    bool_value = bool(re.search(r'\[\!?[^\]]+\]|\*|\?', arg))
    #debug(f'is_pattern(arg={arg!r}): {bool_value}')
    return bool_value


def get_host(hostname: str, args: argparse.Namespace) -> Host:
    '''get host config for given hostname

    args must contain (but may be empty strings):
    - args.config
    - args.username
    - args.askpassword
    - args.password
    - args.cafile

    Returns Host instance
    May raise ValueError for unknown host
    '''

    # try getting the fqdn of a given hostname
    # if the given hostname is not in DNS, then fqdn will be the same string
    fqdn = socket.getfqdn(hostname)
    if fqdn != hostname:
        debug(f'resolved {hostname} => {fqdn}')

    hostmap = load_config(args.config)

    host = Host()
    try:
        # try matching exactly the given hostname
        # this may be a short hostname
        host = hostmap[hostname].host
        debug(f'matches Host {hostname} ({hostmap[hostname].filename}:{hostmap[hostname].lineno})')
    except KeyError:
        try:
            # try matching exactly the fqdn
            host = hostmap[fqdn].host
            debug(f'matches Host {fqdn} ({hostmap[fqdn].filename}:{hostmap[fqdn].lineno})')
        except KeyError:
            # the config may contain wildcards
            # try matching the resolved hostname (fqdn) against those wildcards
            found = False
            for pattern, entry in hostmap.items():
                if pattern == '*':
                    # skip default for now
                    continue
                if fnmatch.fnmatch(fqdn, pattern):
                    debug(f'hostname {fqdn} matches Host {pattern} ({entry.filename}:{entry.lineno})')
                    host = entry.host
                    found = True
                    break

            if not found:
                # try matching given hostname against patterns
                for pattern, entry in hostmap.items():
                    if pattern == '*':
                        # skip default for now
                        continue
                    if fnmatch.fnmatch(hostname, pattern):
                        debug(f'hostname {hostname} matches Host {pattern} ({entry.filename}:{entry.lineno})')
                        host = entry.host
                        found = True
                        break

            if not found:
                # try default 'Host *' (if it is configured)
                try:
                    host = hostmap['*'].host
                    debug(f'hostname {fqdn} matches Host *')
                except KeyError:
                    host = Host()

    host_obj = copy.deepcopy(host)

    # apply any defaults from 'Host *' (if such an entry exists)
    try:
        host_defaults = hostmap['*'].host
        host_obj.apply_defaults_from(host_defaults)
    except KeyError:
        pass

    debug(f'host_obj == {host_obj!r}')
    if not host_obj.host:
        host_obj.host = fqdn

    # check that the host exists
    _ = dns_resolve_host(host_obj.host)

    if not host_obj.username:
        # set default username
        host_obj.username = 'root'

    # parameters may have been passed on the command-line
    if args.username:
        host_obj.username = args.username

    if args.password:
        host_obj.password = args.password

    if args.askpassword or not host_obj.password:
        host_obj.password = get_interactive_password()

    if args.cafile:
        host_obj.cafile = args.cafile

    debug(f'returning {host_obj!r}')
    return host_obj


def get_interactive_password() -> str:
    '''Get password from keyboard input'''

    return getpass.getpass()


def dns_resolve_host(host: str) -> List[str]:
    '''Returns list of IP addresses
    May raise ValueError for unknown host
    '''

    try:
        addrs = socket.getaddrinfo(host, None, proto=socket.IPPROTO_TCP)
        ipaddrs = [x[4][0] for x in addrs]
        debug(f'ipaddrs == {ipaddrs!r}')
        return ipaddrs

    except socket.gaierror as err:
        raise ValueError(f'unknown host: {host}') from err


# EOB
