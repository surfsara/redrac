#
#   redrac redfishcode.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''redfish wrapper classes'''

import sys
import http
import json
import time
import re

from dataclasses import dataclass, field
from typing import List, Dict, Tuple, Union, Optional, Any

import redfish

from redrac.debug import debug
from redrac.print import print                                      # pylint: disable=redefined-builtin
from redrac.idict import idict


class RedfishError(Exception):
    '''Redfish related error'''



class InvalidJSONError(Exception):
    '''invalid JSON in response body'''



@dataclass
class Response:
    '''represents a HTTP response'''

    status_code: int = 0
    headers: idict = field(default_factory=idict)
    text: str = ''

    @staticmethod
    def from_redfish_response(redfish_response: redfish.rest.v1.RestResponse) -> 'Response':        # pyright: ignore[reportAttributeAccessIssue]
        '''Returns a new Response, made from a redfish module's response instance'''

        response = Response()
        response.status_code = redfish_response.status

        # NOTE this may raise UnicodeDecodeError
        response.text = redfish_response.text

        looks_like_int = re.compile(r'\d+$')

        headers_idict = idict()
        for header, value in redfish_response.getheaders():
            try:
                if looks_like_int.match(value) is not None:
                    headers_idict[header] = int(value)
                else:
                    headers_idict[header] = value
            except (ValueError, TypeError):
                headers_idict[header] = value

        response.headers = headers_idict
        return response

    @property
    def ok(self) -> bool:
        '''Returns True if status_code is in category "Successful"'''

        return 200 <= self.status_code < 300

    def json(self) -> Dict[str, Any]:
        '''Returns JSON decoded content
        May raise InvalidJSONError for invalid JSON
        '''

        if not self.text:
            return {}

        try:
            return json.loads(self.text)
        except (json.JSONDecodeError, UnicodeDecodeError, TypeError) as err:
            debug('invalid JSON in response:')
            debug(self.text)
            raise InvalidJSONError from err

    def __str__(self) -> str:
        '''Returns string'''

        try:
            content_type = self.headers['Content-Type']
            if 'json' in content_type:
                json_data = self.json()
                return json.dumps(json_data, indent=2)
        except KeyError:
            pass
        return self.text



class RedfishClient:
    '''represents a Redfish client'''

    def __init__(self) -> None:
        '''initialize instance'''

        self.host = ''
        self.username = ''
        self.redfish_obj = None             # type: Optional[redfish.rest.v1.HttpClient]            # pyright: ignore[reportAttributeAccessIssue]
        self.path_cache = {}                # type: Dict[str, List[str]]

    def __enter__(self) -> 'RedfishClient':
        '''enter managed context'''

        return self

    def __exit__(self, exc_type, exc_value, exc_traceback) -> None:
        '''leave managed context'''

        if self.redfish_obj is not None:
            self.logout()

    @staticmethod
    def login(host: str, username: str, password: str, cafile: str = '') -> 'RedfishClient':
        '''login to remote controller
        Returns a RedfishClient instance
        that can be used as a context manager:

            with RedfishClient.login(host, username, password) as redfish_obj:
                ...
        '''

        if not cafile:
            cafile = None               # type: ignore

        obj = RedfishClient()
        obj.redfish_obj = redfish.redfish_client(base_url='https://' + host, username=username, password=password,
                                                 default_prefix='/redfish/v1', timeout=10, cafile=cafile)
        assert obj.redfish_obj is not None
        obj.redfish_obj.login(auth='session')
        obj.host = host
        obj.username = username
        return obj

    def logout(self) -> None:
        '''logout of session'''

        assert self.redfish_obj is not None

        self.redfish_obj.logout()
        self.host = ''
        self.username = ''
        self.redfish_obj = None

    @staticmethod
    def format_json(d: Dict[str, Any]) -> str:
        '''Returns pretty-printed JSON string'''

        return json.dumps(d, indent=2)

    @staticmethod
    def format_http_status(code: int) -> str:
        '''format HTTP status code as text
        Returns text string
        '''

        return f'{code} {http.HTTPStatus(code).phrase}'

    @staticmethod
    def make_redfish_attributes(response: Response) -> Dict[str, Any]:
        '''Returns dictionary containing Redfish attributes
        May raise InvalidJSONError
        '''

        # this may raise InvalidJSONError
        response_dict = response.json()

        # put response header "Location" into the response dict (if it exists)
        if 'Location' not in response_dict:
            try:
                response_dict['Location'] = response.headers['Location']
            except KeyError:
                pass

        # put response header "Retry-After" into the response dict (if it exists)
        if 'Retry-After' not in response_dict:
            try:
                response_dict['Retry-After'] = int(response.headers['Retry-After'])
            except KeyError:
                pass
            except ValueError:
                retry_after = response.headers['Retry-After']
                debug(f'Retry-After: invalid integer value: {retry_after}')

        return response_dict

    def get(self, url: str) -> Dict[str, Any]:
        '''do a Redfish GET request
        Returns dictionary with Redfish attributes
        May raise RedfishError
        '''

        response = self.http_get(url)
        if response.status_code not in (http.HTTPStatus.OK, http.HTTPStatus.ACCEPTED):
            raise RedfishError(f'{self.format_http_status(response.status_code)} GET {url}')

        response_dict = self.make_redfish_attributes(response)
        debug(f'response_dict == {response_dict!r}')

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict

    def get_headers(self, url: str) -> Tuple[Dict[str, Any], idict]:
        '''do a Redfish GET request
        Returns tuple: (dictionary with Redfish attributes, dictionary with headers)
        May raise RedfishError
        '''

        response = self.http_get(url)
        if response.status_code not in (http.HTTPStatus.OK, http.HTTPStatus.ACCEPTED):
            raise RedfishError(f'{self.format_http_status(response.status_code)} GET {url}')

        debug(f'headers == {response.headers!r}')

        response_dict = self.make_redfish_attributes(response)
        debug(f'response_dict == {response_dict!r}')

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict, response.headers

    def http_get(self, url: str) -> Response:
        '''do a HTTP GET request
        Returns a Response instance
        '''

        assert self.redfish_obj is not None

        debug(f'GET {url}')
        redfish_response = self.redfish_obj.get(url)
        response = Response.from_redfish_response(redfish_response)
        debug(f'response.status_code == {self.format_http_status(response.status_code)}')
        return response

    def post(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''do a Redfish POST request
        Returns dictionary with response data
        May raise RedfishError
        '''

        response = self.http_post(url, body, headers)

        debug(f'headers == {response.headers!r}')

        response_dict = self.make_redfish_attributes(response)
        debug(f'response_dict == {response_dict!r}')

        if response.status_code not in (http.HTTPStatus.OK, http.HTTPStatus.NO_CONTENT,
                                        http.HTTPStatus.ACCEPTED, http.HTTPStatus.CREATED):
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if response.status_code == http.HTTPStatus.ACCEPTED:
            # HTTP 202 Accepted; asynchronous task in server
            task = RedfishTask.from_client_response(self, response)
            response = task.monitor()
            try:
                response_dict = response.json()
            except InvalidJSONError:
                # invalid JSON in response body
                debug('invalid JSON in response body; returning {Body: text}')
                response_dict = {'Body': response.text}

        debug(self.get_message(response_dict))
        return response_dict

    def http_post(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Response:
        '''do a HTTP POST request
        Returns a Response instance
        '''

        assert self.redfish_obj is not None

        headers = self._make_headers(body, headers)

        debug(f'POST {url} body == {body!r} headers == {headers!r}')
        redfish_response = self.redfish_obj.post(url, body=body, headers=headers)
        response = Response.from_redfish_response(redfish_response)
        debug(f'response.status_code == {self.format_http_status(response.status_code)}')
        return response

    @staticmethod
    def _make_headers(body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''make request headers
        Automatically set Content-Type: application/json if needed

        Returns headers dictionary
        '''

        if headers is None:
            if body:
                # there is a body; provide content-type header
                return {'Content-Type': 'application/json'}
            return {}
        assert headers is not None      # this helps mypy
        if body:
            if 'Content-Type' not in headers:
                headers['Content-Type'] = 'application/json'
        return headers

    # def put(self, )

    # def head(self, )

    def patch(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Dict[str, Any]:
        '''do a Redfish PATCH request
        Returns dictionary with response data
        May raise RedfishError
        '''

        response = self.http_patch(url, body, headers)

        response_dict = self.make_redfish_attributes(response)
        debug(f'response_dict == {response_dict!r}')

        if response.status_code not in (http.HTTPStatus.OK, http.HTTPStatus.ACCEPTED, http.HTTPStatus.NO_CONTENT):
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict

    def http_patch(self, url: str, body: Dict[str, Any], headers: Optional[Dict[str, Any]] = None) -> Response:
        '''do a HTTP PATCH request
        Returns a Response instance
        '''

        assert self.redfish_obj is not None

        headers = self._make_headers(body, headers)

        debug(f'PATCH {url} body == {body!r} headers == {headers!r}')
        redfish_response = self.redfish_obj.patch(url, body=body, headers=headers)
        response = Response.from_redfish_response(redfish_response)
        debug(f'response.status_code == {self.format_http_status(response.status_code)}')
        return response

    def delete(self, url: str) -> Dict[str, Any]:
        '''do a Redfish DELETE request
        Returns dictionary with response data
        May raise RedfishError
        '''

        response = self.http_delete(url)

        response_dict = self.make_redfish_attributes(response)
        debug(f'response_dict == {response_dict!r}')

        if response.status_code not in (http.HTTPStatus.OK, http.HTTPStatus.ACCEPTED, http.HTTPStatus.NO_CONTENT):
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        if 'error' in response_dict:
            # sad but true; we might get HTTP OK, but still have an error condition
            err_msg = self.get_extended_error(response_dict)
            raise RedfishError(err_msg)

        debug(self.get_message(response_dict))
        return response_dict

    def http_delete(self, url: str) -> Response:
        '''do a HTTP DELETE request
        Returns a Response instance
        '''

        assert self.redfish_obj is not None

        debug(f'DELETE {url}')
        redfish_response = self.redfish_obj.delete(url, None)
        response = Response.from_redfish_response(redfish_response)
        debug(f'response.status_code == {self.format_http_status(response.status_code)}')
        return response

    @staticmethod
    def get_extended_error(response: Dict[str, Any]) -> str:
        '''get extended error message from Redfish response'''

        try:
            return RedfishClient.get_message(response['error'])

        except KeyError:
            return str(response)

    @staticmethod
    def get_message(response: Dict[str, Any]) -> str:
        '''get extended message from Redfish response'''

        try:
            extended_info = response['@Message.ExtendedInfo']

            if isinstance(extended_info, list):
                # join all messages in the list
                return '\n'.join([x['Message'] for x in extended_info]).rstrip()

            # it is a dict containing a single message
            return extended_info['Message']

        except KeyError:
            return ''

    def query_urls(self, path: str) -> List[str]:
        '''Returns list of URLs for pseudo-path

        pseudo-path is something like:
            "Chassis:Members:ThermalSubsystem:Fans:Members"

        May raise RedfishError, KeyError for invalid path
        '''

        if not path:
            raise RuntimeError('invalid argument')

        try:
            url_list = self.path_cache[path]
            debug(f'cache hit; path {path!r} => {url_list!r}')
            return url_list

        except KeyError:
            debug(f'cache miss; path == {path!r}')

        url_list = self._do_query_urls(path)
        self._store_cache(path, url_list)
        return url_list

    def _do_query_urls(self, path: str, url: str = '/redfish/v1', done: Optional[List[str]] = None, queue: Optional[List[str]] = None) -> List[str]:
        '''Returns list of URLs for pseudo-path

        pseudo-path is something like:
            "Chassis:Members:ThermalSubsystem:Fans:Members"

        May raise RedfishError, KeyError for invalid path
        '''

        # This method is recursive, and works in tandem with a helper
        # function _do_query_urls_attrib() (see below)
        # Mind that the list arguments to this method are reference types;
        # the lists are manipulated while the recursion continues
        # The helper function may recursively call itself, or call this method
        #
        # `done` is a list of path elements that have been walked already
        # `queue` is a list of path elements to be walked

        debug(f'url == {url!r}')

        if done is None:
            done = []
        debug(f'done == {done!r}')

        if queue is None:
            queue = path.split(':')
        debug(f'queue == {queue!r}')

        if not queue:
            # reached end of path
            url_list = [url,]
        else:
            # HTTP GET the URL
            # this is the right moment to cache the path
            # mind that because of recursion, `path` is not the correct path
            # for this particular URL (therefore use `current_path`)
            current_path = ':'.join(done)
            self._store_cache(current_path, [url,])

            data = self.get(url)
            try:
                # warning: `done` and `queue` get modified by this subroutine
                url_list = self._do_query_urls_attrib(data, done, queue)
            except KeyError as err:
                # this may happen for members that do not have a sub path
                raise KeyError(f'invalid path: {path!r}') from err

        debug(f'return url_list {url_list!r}')
        return url_list

    def _store_cache(self, path: str, url_list: List[str]) -> None:
        '''store the url_list in cache under path'''

        if not path:
            return

        # store/update and remove duplicate URLs

        if path in self.path_cache:
            self.path_cache[path].extend(url_list)
        else:
            self.path_cache[path] = url_list
        self.path_cache[path] = list(set(self.path_cache[path]))
        debug(f'store cache: {path!r} := {self.path_cache[path]}')

    def _do_query_urls_attrib(self, attrib: Union[Dict[str, Any], List[Dict[str, Any]]], done: List[str], queue: List[str]) -> List[str]:
        '''helper function for query_urls()
        Examine the attribute
        Returns list of URLs
        May raise KeyError for invalid path
        '''

        debug(f'attrib == {attrib!r}')

        url_list = []

        # NOTE do "late binding" for the variable `path`
        # the list `done` represents the current path
        # but mind that this list is being modified recursively
        # and therefore changes all the time
        # Therefore remake the `path` every time before printing/using it

        if attrib is None:
            path = ':'.join(done)
            raise KeyError(f'invalid path: {path!r}')

        if isinstance(attrib, dict):
            if len(attrib) == 1 and '@odata.id' in attrib:
                url = attrib['@odata.id']
                if not queue:
                    url_list.append(url)
                    path = ':'.join(done)
                    debug(f'return (1) path {path!r} => {url_list!r}')
                    return url_list
                path = ':'.join(done)
                return self._do_query_urls(path, url, done, queue)

            # try the next path element
            try:
                tag = queue[0]
            except IndexError:
                path = ':'.join(done)
                debug(f'return (2) path {path!r} => {url_list!r}')
                return url_list

            try:
                item = attrib[tag]
            except KeyError as err:
                path = ':'.join(done)
                raise KeyError(f'invalid path: {path!r}') from err

            tag = queue.pop(0)
            done.append(tag)

            # recurse
            try:
                url_list.extend(self._do_query_urls_attrib(item, done, queue))
            except KeyError as err:
                path = ':'.join(done)
                raise KeyError(f'invalid path: {path!r}') from err

            # goto return (attrib is dict)

        elif isinstance(attrib, list):
            item_success = False
            queue_len = 0
            for item in attrib:
                # a single list item should be a dictionary
                # if it is not a dict, then it is not a Redfish object -> error
                if not isinstance(item, dict):
                    path = ':'.join(done)
                    raise KeyError(f'invalid path: {path!r}')

                try:
                    queue_copy = queue[:]
                    done_copy = done[:]
                    url_list.extend(self._do_query_urls_attrib(item, done_copy, queue_copy))

                    if not item_success:
                        # Note: this assumes that all members have similar subtrees
                        item_success = True
                        queue_len = len(queue_copy)

                except KeyError:
                    # this may happen for members that do not have a sub path
                    pass

            if item_success:
                # path elements were eaten off the queue
                # fixup queue
                while len(queue) > queue_len:
                    tag = queue.pop(0)
                    done.append(tag)

            # goto return (attrib is list)

        elif isinstance(attrib, str):
            if attrib.startswith('/redfish/'):
                url = attrib
                if not queue:
                    url_list.append(url)
                    path = ':'.join(done)
                    debug(f'return (3) path {path!r} => {url_list!r}')
                    return url_list
                path = ':'.join(done)
                return self._do_query_urls(path, url, done, queue)

            path = ':'.join(done)
            raise KeyError(f'invalid path: {path}')

        else:
            # different type (int, float, bool, ?)
            path = ':'.join(done)
            raise KeyError(f'invalid path: {path}')

        path = ':'.join(done)
        debug(f'path == {path!r}')
        debug(f'queue == {queue!r}')
        debug(f'done == {done!r}')

        url_list = list(set(url_list))
        debug(f'return path {path!r} => {url_list!r}')
        return url_list

    def load(self, path: str) -> List[Dict[str, Any]]:
        '''fetch Redfish data by pseudo-path

        pseudo-path is something like:
            "Chassis:Members:ThermalSubsystem:Fans:Members"

        Returns list of dictionaries (a list of Redfish objects)

        May raise RedfishError, KeyError for invalid path
        '''

        urls = self.query_urls(path)
        debug(f'path == {path!r}  urls == {json.dumps(urls, indent=2)}')

        objs = []
        for url in urls:
            objs.append(self.get(url))
        return objs



class RedfishTask:
    '''represents a Redfish task
    A task is an asynchronous server process

    (This class gets used by RedfishClient, usually one does not instantiate
    this class yourself)
    '''

    def __init__(self, url: str, client: RedfishClient) -> None:
        '''initialize instance'''

        self.url = url
        self.client = client
        self.state = ''                 # Running|Completed|... etc.
        self.percent = -1
        self.was_updated = False        # flag indicating change in state|percentage
        self.retry_after = 0            # Retry-After response header; time in seconds
        self.response = Response()      # last response
        self.messages = []              # type: List[Dict[str, Any]]    # any messages after the task ended

    @staticmethod
    def from_client_response(client: RedfishClient, response: Response) -> 'RedfishTask':
        '''Returns a new RedfishTask instance
        May raise ValueError
        '''

        try:
            task_url = response.headers['Location']
        except KeyError as err:
            raise ValueError('Redfish task id not found') from err

        task = RedfishTask(task_url, client)

        try:
            task.retry_after = int(response.headers['Retry-After'])
        except KeyError:
            pass
        except ValueError as err:
            retry_after = response.headers['Retry-After']
            raise ValueError(f'Retry-After: invalid integer value: {retry_after!r}') from err

        return task

    @staticmethod
    def format_json(d: Dict[str, Any]) -> str:
        '''Returns pretty-printed JSON string'''

        return json.dumps(d, indent=2)

    def monitor(self) -> Response:
        '''wait for asynchronous task to complete
        Returns the final response after the task ended

        This method prints messages to stdout

        May raise RedfishError
        '''

        if not self.url:
            raise ValueError('invalid task url')

        self.state = ''
        self.percent = -1
        self.was_updated = False
        self.response = Response()
        self.messages = []

        pending_count = 0

        while self.is_running():
            if self.was_updated:
                self.clear_line()
                print(f'{self} ', end='', flush=True)

            if self.state == 'Pending':
                pending_count += 1
                if pending_count > 3:
                    debug('task stays in Pending state, not waiting for it any longer')
                    break

        # print final state
        self.clear_line()
        print(f'{self}')
        # print extra server messages (if any)
        self.print_messages()
        return self.response

    @staticmethod
    def clear_line() -> None:
        '''clear line of output'''

        if sys.stdout.isatty():
            print('\r\x1b[2K', end='', flush=True)
        else:
            print()

    def print_messages(self) -> None:
        '''print any messages to stdout'''

        for msg_dict in self.messages:
            try:
                print(msg_dict['Message'])
            except KeyError:
                pass

    S_RUNNING = ('New', 'Pending', 'Service', 'Starting', 'Stopping', 'Running', 'Cancelling', 'Verifying')
    S_END = ('Cancelled', 'Completed', 'Exception', 'Killed', 'Interrupted', 'Suspended')

    def is_running(self) -> bool:
        '''Returns True while the task has not ended
        May raise RedfishError
        '''

        if self.retry_after > 0:
            debug(f'Retry-After: {self.retry_after} seconds')
            time.sleep(self.retry_after)
        else:
            if self.state:
                # we are not in the starting state
                # and do not have a 'Retry-After' sleep
                time.sleep(1.2)

        self.response = self.client.http_get(self.url)
        self.handle_response()

        if self.state in RedfishTask.S_RUNNING or self.state.startswith('Downloading') or self.state.startswith('Update'):
            return True

        if self.state in RedfishTask.S_END:
            return False

        raise RedfishError(f'invalid task state {self.state!r}')

    def is_success(self) -> bool:
        '''Returns True if task completed successfully'''

        return self.state == 'Completed'

    def get_output(self) -> Union[str, Dict[str, Any]]:
        '''Returns task output

        The return type is either a string or a dictionary

        You should only call this function for completed tasks
        '''

        try:
            output_dict = self.response.json()
            return output_dict

        except InvalidJSONError:
            return self.response.text

    def handle_response(self) -> None:
        '''update and handle the state of this asynchronous task

        This method prints messages to stdout

        May raise RedfishError
        '''

        self.was_updated = False

        try:
            response = self.response.json()
        except InvalidJSONError:
            # invalid JSON; ugh
            debug('invalid JSON in response')
            response = {}

        try:
            task_state = response['TaskState']
        except KeyError:
            # we are expecting a task state, but it is not there
            # assume that it is now completed
            # which is ugly, but appears to be a good workaround
            debug('missing TaskState; assuming task was completed')
            task_state = 'Completed'

        if task_state != self.state:
            debug(f'task_state == {task_state!r}')
            self.state = task_state
            self.was_updated = True

        try:
            task_percent = response['PercentComplete']
        except KeyError:
            try:
                task_percent = response['Oem']['Dell']['PercentComplete']
            except KeyError:
                task_percent = self.percent
        if task_percent is not None and task_percent != self.percent:
            debug(f'task_percent == {task_percent!r}')
            self.percent = task_percent
            self.was_updated = True

        if task_state == 'Completed' and self.percent != -1:
            # force it to 100% for display neatness
            self.percent = 100
            self.was_updated = True

        if task_state in RedfishTask.S_END:
            # there may be additional messages;
            try:
                self.messages = response['Messages']
                debug(f'task messages == {self.messages!r}')
            except KeyError:
                pass

    def __str__(self) -> str:
        '''Returns string representing the task state
        Note: the first state is an empty string
        '''

        lower_state = self.state.lower()

        if self.state.startswith('Downloading') or self.state.startswith('Update'):
            if self.percent > -1:
                return f'{self.percent}% {lower_state}'

        if self.state in RedfishTask.S_RUNNING:
            if self.percent > -1:
                return f'{self.percent}% {lower_state}'
            return f'{lower_state} task'

        if self.state in RedfishTask.S_END:
            return f'task {lower_state}'

        return lower_state

# EOB
