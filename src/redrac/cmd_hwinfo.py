#
#   redrac cmd_hwinfo.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: hwinfo

Show hardware information
'''

import argparse

from typing import Dict, Any

from redrac import progress
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient
from redrac.util import print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


class Manager:
    '''represents a Redfish Manager (ie. remote access controller)'''

    def __init__(self, model: str = '(unknown)', manager_type: str = '') -> None:
        '''initialize instance'''

        self.model = model
        self.type = manager_type

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Manager':
        '''instantiate from data dictionary'''

        try:
            model = data['Model']
        except KeyError:
            model = '(unknown)'

        try:
            manager_type = data['ManagerType']
        except KeyError:
            manager_type = ''

        return Manager(model, manager_type)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Model': self.model,
                'ManagerType': self.type}

    def __str__(self) -> str:
        '''Returns string'''

        a = []
        if self.model:
            a.append(self.model)
        if self.type:
            a.append(self.type)

        s = ' '.join(a)
        if not s:
            return '(unknown)'
        return s



class Processor:
    '''represents a Redfish Processor'''

    def __init__(self, manufacturer: str = '', model: str = '(unknown)', arch: str = '', processor_type: str = 'CPU', cores: int = 1, enabled_cores: int = 1) -> None:
        '''initialize instance'''

        self.manufacturer = manufacturer
        self.model = model
        self.arch = arch
        self.type = processor_type
        self.cores = cores
        self.enabled_cores = enabled_cores

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Processor':
        '''instantiate from data dictionary'''

        try:
            manufacturer = data['Manufacturer']
        except KeyError:
            manufacturer = ''

        try:
            model = data['Model']
        except KeyError:
            model = '(unknown)'

        try:
            arch = data['ProcessorArchitecture']
        except KeyError:
            arch = ''

        try:
            processor_type = data['ProcessorType']
        except KeyError:
            processor_type = 'CPU'

        try:
            cores = int(data['TotalCores'])
        except (KeyError, ValueError, TypeError):
            cores = 1

        try:
            enabled_cores = int(data['EnabledCores'])
        except (KeyError, ValueError, TypeError):
            enabled_cores = cores

        return Processor(manufacturer, model, arch, processor_type, cores, enabled_cores)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Model': self.model,
                'Manufacturer': self.manufacturer,
                'ProcessorArchitecture': self.arch,
                'ProcessorType': self.type,
                'TotalCores': self.cores,
                'EnabledCores': self.enabled_cores}

    def __str__(self) -> str:
        '''Returns string'''

        a = []
        if self.manufacturer:
            a.append(self.manufacturer)
        if self.model:
            a.append(self.model)
        if self.arch:
            a.append(self.arch)
        if self.type != 'CPU':
            a.append(f'({self.type})')

        if self.cores > 1:
            if self.enabled_cores == self.cores:
                a.append(f'{self.cores} cores')
            else:
                a.append(f'{self.enabled_cores}/{self.cores} cores')

        s = ' '.join(a)
        if not s:
            return '(unknown)'
        return s



class Memory:
    '''represents Redfish Memory'''

    def __init__(self, name: str = '(unknown)', capacity_mib: int = 0, memory_type: str = '', speed_mhz: int = 0, manufacturer: str = '') -> None:
        '''initialize instance'''

        self.name = name
        self.capacity_mib = capacity_mib
        self.type = memory_type
        self.speed_mhz = speed_mhz
        self.manufacturer = manufacturer

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Memory':
        '''instantiate from data dictionary'''

        try:
            name = data['Name']
        except KeyError:
            name = ''

        try:
            capacity_mib = int(data['CapacityMiB'])
        except (KeyError, ValueError, TypeError):
            capacity_mib = 0

        try:
            memory_type = data['MemoryDeviceType']
        except KeyError:
            memory_type = ''

        try:
            speed_mhz = int(data['OperatingSpeedMhz'])
        except (KeyError, ValueError, TypeError):
            speed_mhz = 0

        try:
            manufacturer = data['Manufacturer']
        except KeyError:
            manufacturer = ''

        return Memory(name, capacity_mib, memory_type, speed_mhz, manufacturer)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Name': self.name,
                'CapacityMiB': self.capacity_mib,
                'MemoryDeviceType': self.type,
                'OperatingSpeedMHz': self.speed_mhz,
                'Manufacturer': self.manufacturer}

    def __str__(self) -> str:
        '''Returns string'''

        a = []
        if self.name:
            a.append(self.name)
        if self.capacity_mib:
            a.append(f'{self.capacity_mib} MiB')
        if self.type:
            a.append(self.type)
        if self.speed_mhz:
            a.append(f'{self.speed_mhz} MHz')
        if self.manufacturer:
            a.append(self.manufacturer)

        s = ' '.join(a)
        if not s:
            s = '(unknown)'
        return s



class Disk:
    '''represents a Redfish Drive'''

    def __init__(self, manufacturer: str = '', model: str = '', name: str = '(name)', capacity_bytes: int = 0, protocol: str = '', media_type: str = '') -> None:
        '''initialize instance'''

        self.manufacturer = manufacturer
        self.model = model
        self.name = name
        self.capacity_bytes = capacity_bytes
        self.protocol = protocol
        self.media_type = media_type

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Disk':
        '''instantiate from data dictionary'''

        try:
            manufacturer = data['Manufacturer']
        except KeyError:
            manufacturer = ''

        try:
            model = data['Model']
        except KeyError:
            model = ''

        try:
            name = data['Name']
        except KeyError:
            name = ''

        try:
            capacity_bytes = int(data['CapacityBytes'])
        except (KeyError, ValueError, TypeError):
            capacity_bytes = 0

        try:
            protocol = data['Protocol']
        except KeyError:
            protocol = ''

        try:
            media_type = data['MediaType']
        except KeyError:
            media_type = ''

        return Disk(manufacturer, model, name, capacity_bytes, protocol, media_type)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Manufacturer': self.manufacturer,
                'Model': self.model,
                'Name': self.name,
                'CapacityBytes': self.capacity_bytes,
                'Protocol': self.protocol,
                'MediaType': self.media_type}

    def __str__(self) -> str:
        '''Returns string'''

        a = []
        if self.capacity_bytes > 0:
            if self.capacity_bytes >= 10**12:
                a.append(f'{self.capacity_bytes / 10**12:.1f} TB')
            else:
                a.append(f'{self.capacity_bytes // 10**9} GB')
        if self.manufacturer:
            a.append(self.manufacturer)
        if self.protocol:
            a.append(self.protocol)
        if self.media_type:
            a.append(self.media_type)
        if self.model:
            a.append(self.model)
        if self.name:
            a.append(self.name)

        s = ' '.join(a)
        if not s:
            s = '(unknown)'
        return s



class Device:
    '''represents a Redfish PCIeDevice'''

    def __init__(self, manufacturer: str = '', description: str = '', name: str = '(unknown)') -> None:
        '''initialize instance'''

        self.manufacturer = manufacturer
        self.description = description
        self.name = name

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Device':
        '''instantiate from data dictionary'''

        try:
            manufacturer = data['Manufacturer']
        except KeyError:
            manufacturer = ''

        try:
            description = data['Description']
            if description.startswith('This resource represents '):
                description = ''
        except KeyError:
            description = ''

        try:
            name = data['Name']
        except KeyError:
            name = ''

        return Device(manufacturer, description, name)

    def as_dict(self) -> Dict[str, Any]:
        '''Returns this instance as dictionary'''

        return {'Manufacturer': self.manufacturer,
                'Description': self.description,
                'Name': self.name}

    def __str__(self) -> str:
        '''Returns string'''

        a = []
        if self.manufacturer and self.manufacturer not in self.name:
            a.append(self.manufacturer)
        if self.description and self.description != self.name:
            a.append(self.description)
        if self.name:
            a.append(self.name)

        s = ' '.join(a)
        if not s:
            s = '(unknown)'
        return s



def do_hwinfo(host: str, args: argparse.Namespace) -> None:
    '''print hardware inventory'''

    data = load_hwinfo(host, args)

    if args.json:
        print_data(data, as_json=True)
        return

    hwinfo = hwinfo_from_data(data)

    manager = hwinfo['Manager']
    if manager is None:
        print('Manager: (none)')
    else:
        print(f'Manager: {manager}')

    if not hwinfo['CPU']:
        print('CPU: (none)')
    elif len(hwinfo['CPU']) > 1:
        print('CPU: ')
        for processor in hwinfo['CPU']:
            print(f'  {processor}')
    else:
        processor = hwinfo['CPU']
        print(f'CPU: {processor}')

    if not hwinfo['Memory']:
        print('Memory: (none)')
    elif len(hwinfo['Memory']) > 1:
        print('Memory: ')
        for memory in hwinfo['Memory']:
            print(f'  {memory}')
    else:
        memory = hwinfo['Memory']
        print(f'Memory: {memory}')

    if not hwinfo['Disk']:
        print('Disk: (none)')
    elif len(hwinfo['Disk']) > 1:
        print('Disk: ')
        for disk in sorted(hwinfo['Disk'], key=lambda x: x.name):
            print(f'  {disk}')
    else:
        disk = hwinfo['Disk']
        print(f'Disk: {disk}')

    if not hwinfo['Devices']:
        print('Devices: (none)')
    elif len(hwinfo['Devices']) > 1:
        print('Devices: ')
        for device in sorted(hwinfo['Devices'], key=lambda x: x.name):
            print(f'  {device}')
    else:
        device = hwinfo['Devices']
        print(f'Devices: {device}')


def load_hwinfo(host: str, args: argparse.Namespace) -> Dict[str, Any]:
    '''fetch Redfish data with hardware information
    Returns dictionary containing Redfish data
    '''

    data = {'Manager': None,
            'CPU': [],
            'Memory': [],
            'Disk': [],
            'Devices': []}

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            data['Manager'] = redfish_obj.load('Managers:Members')
        except KeyError:
            # invalid pseudo-path
            data['Manager'] = None

        try:
            data['CPU'] = redfish_obj.load('Systems:Members:Processors:Members')
        except KeyError:
            data['CPU'] = []

        try:
            data['Memory'] = redfish_obj.load('Systems:Members:Memory:Members')
        except KeyError:
            data['Memory'] = []

        try:
            data['Disk'] = redfish_obj.load('Chassis:Members:Links:Drives')
        except KeyError:
            data['Disk'] = []

        try:
            data['Devices'] = redfish_obj.load('Systems:Members:PCIeDevices')
        except KeyError:
            data['Devices'] = []

    return data


def hwinfo_from_data(data: Dict[str, Any]) -> Dict[str, Any]:
    '''make hwinfo data from Redfish data
    Returns dictionary containing hardware information
    '''

    hwinfo = {'Manager': None,
              'CPU': [],
              'Memory': [],
              'Disk': [],
              'Devices': []}

    if data['Manager'] is not None:
        # use only the first Manager
        hwinfo['Manager'] = Manager.from_data(data['Manager'][0])
    else:
        hwinfo['Manager'] = Manager()

    # note, if there is no CPU/disk/whatever, then the list will be empty
    # (no need to make special if-cases for that here)
    hwinfo['CPU'] = [Processor.from_data(x) for x in data['CPU']]
    hwinfo['Memory'] = [Memory.from_data(x) for x in data['Memory']]
    hwinfo['Disk'] = [Disk.from_data(x) for x in data['Disk']]
    hwinfo['Devices'] = [Device.from_data(x) for x in data['Devices']]
    return hwinfo


# EOB
