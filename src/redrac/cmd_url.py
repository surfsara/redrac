#
#   redrac cmd_url.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: url

get/post/patch/delete Redfish URL
export/import Redfish object via URL
'''

import argparse
import json
import os
import http

from typing import List, Dict, Any

from redrac import progress
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, Response, InvalidJSONError, RedfishTask
from redrac.util import print_data


def do_url(host: str, args: argparse.Namespace) -> None:
    '''get/export/import Redfish object via URL'''

    do_request = args.get or args.post or args.patch or args.delete
    if not do_request:
        if args.import_file:
            # if importing a file, default is a PATCH request
            args.patch = True
        else:
            # default is a GET request
            args.get = True

    if args.export:
        args.json = True

    host, username, password, cafile = get_host(host, args).as_tuple()

    body = {}
    headers = {}
    if args.import_file:
        # --import is a backwards compatible option; same as --body
        body = load_import_file(args.import_file)

    if args.body_file:
        body = load_import_file(args.body_file)

    if args.headers_file:
        headers = load_json_file(args.headers_file)

    if body and not headers:
        # set default headers
        headers = {'If-Match': '*',
                   'Content-Type': 'application/json'}

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        do_url_http_request(redfish_obj, args, body, headers)


def load_json_file(filename: str) -> Dict[str, Any]:
    '''load JSON file
    Returns dictionary
    May raise ValueError, OSError
    '''

    data = {}

    with open(filename, encoding='utf-8') as fh:
        try:
            data = json.load(fh)

            # typecheck; the JSON data can be any kind of object
            if not isinstance(data, dict):
                short_filename = os.path.basename(filename)
                raise ValueError(f'{short_filename}: expected a JSON dictionary')

        except json.decoder.JSONDecodeError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

    return data


def load_import_file(filename: str) -> Dict[str, Any]:
    '''load JSON file to import
    Returns dictionary
    May raise ValueError, OSError
    '''

    # This function is the same as load_json_file() above,
    # but with a different message for the typecheck
    # Duplicate code is not OK, but I don't want to mess around
    # with TypeError here or anything like that

    settings = {}

    with open(filename, encoding='utf-8') as fh:
        try:
            settings = json.load(fh)

            # typecheck; the JSON data can be any kind of object
            if not isinstance(settings, dict):
                short_filename = os.path.basename(filename)
                raise ValueError(f'{short_filename}: expected a Redfish object')

        except json.decoder.JSONDecodeError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

    return settings


def do_url_http_request(redfish_obj: RedfishClient, args: argparse.Namespace, body: Dict[str, Any], headers: Dict[str, Any]) -> None:
    '''perform the url command'''

    if args.get:
        response = redfish_obj.http_get(args.url)
    elif args.post:
        response = redfish_obj.http_post(args.url, body, headers)
    elif args.patch:
        response = redfish_obj.http_patch(args.url, body, headers)
    elif args.delete:
        response = redfish_obj.http_delete(args.url)
    else:
        # this should never happen
        # anyway, do a GET request
        response = redfish_obj.http_get(args.url)

    try:
        data = RedfishClient.make_redfish_attributes(response)
    except InvalidJSONError:
        print(response.text)
        return

    if args.attrib is not None:
        data = filter_attributes(data, args.attrib)

    if args.template:
        args.json = True
        data = make_template(data, args)
    else:
        # if we have a "Location" header, then show it
        # same with "Retry-After"
        if 'Location' in response.headers or 'Retry-After' in response.headers:
            data = response.json()
            try:
                data['Location'] = response.headers['Location']
            except KeyError:
                pass
            try:
                data['Retry-After'] = response.headers['Retry-After']
            except KeyError:
                pass

    if args.wait and response.status_code == http.HTTPStatus.ACCEPTED and 'Location' in response.headers:
        data = wait_for_task_completion(redfish_obj, response)
        if not data:
            # messy fix for printing empty dict as JSON, when we already printed XML data
            return

    print_data(data, as_json=args.json)


def filter_attributes(data: Dict[str, Any], attribs: List[str]) -> Dict[str, Any]:
    '''Returns new dictionary containing only given attributes'''

    # case-insensitive matching
    insensitive_attribs = set(x.lower() for x in attribs)
    return {x: data[x] for x in data.keys() if x.lower() in insensitive_attribs}


def make_template(body: Dict[str, Any], args: argparse.Namespace) -> Dict[str, Any]:
    '''Returns request as a redrac command (in dictionary form)'''

    data = {'comment': 'redrac JSON command',
            'redrac': True,
            'path': '',
            'path-match': '',
            'url': args.url,
            'url-match': '',
            'request': 'PATCH',
            'body': body,
            'headers': {'If-Match': '*'}}

    if args.post:
        data['request'] = 'POST'

    # switch between 'path' and 'url' based on used command-line options
    if hasattr(args, 'path') and args.path:
        data['path'] = args.path
        del data['url']

        if hasattr(args, 'match') and args.match:
            data['path-match'] = args.match

        if hasattr(args, 'url_match') and args.url_match:
            data['url-match'] = args.url_match
    else:
        del data['path']

    if not data['path-match']:
        del data['path-match']

    if not data['url-match']:
        del data['url-match']

    return data


def wait_for_task_completion(redfish_obj: RedfishClient, response: Response) -> Dict[str, Any]:
    '''wait for asynchronous task to complete
    Returns response dictionary

    Note: if the response generates an InvalidJSONError, then
    this function prints the response.text and returns an empty dict
    '''

    task = RedfishTask.from_client_response(redfish_obj, response)
    task.monitor()

    if task.is_success():
        output = task.get_output()

        if isinstance(output, str):
            print(output)
            output = {}
        #else: output is a response dict

    else:
        # task did not successfully complete
        output = {}

    return output


# EOB
