#
#   redrac cmd_biosattr.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: biosattr

Show BIOS attributes
'''

import os
import argparse
import json
import re

from typing import List, Dict, Union, Optional, Any

from redrac import progress
from redrac.debug import debug
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.hostconf import get_host
from redrac.util import print_data, MultiLineError
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_biosattr(host: str, args: argparse.Namespace) -> None:
    '''show BIOS attributes'''

    if args.export:
        # exporting is the same as printing all settings as JSON
        args.json = True
        show_all_bios_attributes(host, args)

    elif args.import_file:
        import_bios_attributes(args.import_file, host, args)

    elif args.attr:
        show_specific_bios_attribute(args.attr, host, args)

    elif args.set:
        change_bios_attribute(args.set, host, args)

    elif args.factory_reset:
        factoryreset_bios(host, args)

    else:
        show_all_bios_attributes(host, args)


def show_all_bios_attributes(host: str, args: argparse.Namespace) -> None:
    '''Show all BIOS attributes'''

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Systems:Members:Bios')

    if args.json:
        # print 'raw' Redfish data
        print_data(data_list, as_json=True)
    else:
        # print attributes as a single dict
        bios_attributes = bios_attributes_from_data(data_list)
        print_data(bios_attributes, as_json=False)


def bios_attributes_from_data(data_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''Convert Redfish data to BIOS attributes dictionary
    Returns dictionary containing the BIOS attributes
    '''

    bios_attributes = {}                # type: Dict[str, Any]
    for data in data_list:
        try:
            bios_attributes.update(data['Attributes'])
        except KeyError:
            pass
    return bios_attributes


def show_specific_bios_attribute(attr: str, host: str, args: argparse.Namespace) -> None:
    '''Show specific BIOS attribute'''

    # NOTE unfortunately we need to get both _all_ attributes and the specific attribute
    # NOTE to get this right. Such is life

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        data_list = redfish_obj.load('Systems:Members:Bios')
        all_bios_attributes = bios_attributes_from_data(data_list)
        bios_attribute = obtain_bios_attribute(redfish_obj, attr)

    # note that attr may be lowercase
    # AttributeName is the (usually camelcase) real name of the attribute
    name = bios_attribute['AttributeName']

    # show the attribute
    # CurrentValue typically appears as `[None, ]` (because it's a schema)
    # patch CurrentValue to be the real current value
    bios_attribute['CurrentValue'] = all_bios_attributes[name]
    print_data(bios_attribute, as_json=args.json)


def obtain_bios_attribute(redfish_obj: RedfishClient, attr: str) -> Dict[str, Any]:
    '''Returns dictionary with BIOS attribute data
    May raise RedfishError, KeyError if no such attribute
    '''

    attribute_list = obtain_bios_attribute_registry(redfish_obj)
    for attribute_dict in attribute_list:
        if attribute_dict['AttributeName'].lower() == attr.lower():
            return attribute_dict

    raise KeyError(f'no such BIOS attribute {attr!r}')


def obtain_bios_attribute_registry(redfish_obj: RedfishClient) -> List[Dict[str, Any]]:
    '''Returns list of dictionaries with BIOS attribute schema data
    May raise RedfishError
    '''

    registry_url = find_bios_attribute_registry(redfish_obj)
    registry_response = redfish_obj.get(registry_url)
    attribute_list = registry_response['RegistryEntries']['Attributes']
    return attribute_list


def find_bios_attribute_registry(redfish_obj: RedfishClient) -> str:
    '''Returns URL to BIOS attribute registry
    May raise RedfishError
    '''

    # the attribute registry is named in a field under Systems/.../Bios
    # the URL to the registry is then Registries/<name>/@odata.id

    attribute_registry = ''

    data_list = redfish_obj.load('Systems:Members:Bios')
    for data in data_list:
        try:
            attribute_registry = data['AttributeRegistry']
            break
        except KeyError:
            pass

    bios_attr_reg_url = ''

    urls = redfish_obj.query_urls('Registries:Members')
    for url in urls:
        name = os.path.basename(url)
        if name == attribute_registry:
            # found the BIOS attribute registry URL
            bios_attr_reg_url = url
            break

    if not bios_attr_reg_url:
        # attribute registry not found
        # unfortunately some Redfish implementations (DELL?) contain an error
        # so that we can not find the registry
        # Therefore we try again with a heuristic
        debug('BIOS attribute registry not found, trying workaround')
        for url in urls:
            name = os.path.basename(url)
            # this is a hack that usually will work
            if name.startswith('BiosAttributeRegistry'):
                bios_attr_reg_url = url
                break

    if bios_attr_reg_url:
        # this URL actually points to yet another location with yet another URL
        response = redfish_obj.get(bios_attr_reg_url)
        try:
            url = response['Location'][0]['Uri']
            debug(f'url == {url}')
            return url
        except (KeyError, IndexError) as err:
            raise RedfishError('BIOS attribute registry not found') from err

    raise RedfishError('BIOS attribute registry not found')


def change_bios_attribute(attr_value: str, host: str, args: argparse.Namespace) -> None:
    '''Change one specific BIOS attribute
    The syntax of `attr_value` is "name=value"

    May raise RedfishError, or ValueError for invalid attribute or value
    '''

    try:
        attr, value = attr_value.split('=', maxsplit=1)
        attr = attr.strip()
        value = value.strip()
    except ValueError as err:
        raise ValueError('usage: biosattr --set ATTR=VALUE') from err

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        set_bios_attributes(redfish_obj, {attr: value})


def get_bios_settings_url(redfish_obj: RedfishClient) -> str:
    '''Returns URL to change BIOS settings
    May raise RedfishError
    '''

    try:
        urls = redfish_obj.query_urls('Systems:Members:Bios:@Redfish.Settings:SettingsObject')
        settings_url = urls[0]
        debug(f'settings_url == {settings_url}')
        return settings_url
    except (KeyError, IndexError) as err:
        raise RedfishError('BIOS settings URL not found; unable to set BIOS attribute') from err


def make_bios_attribute_value(attr: Dict[str, Any], attr_value: Optional[str]) -> Union[Optional[str], int, bool]:
    '''Returns value to set in the BIOS attribute
    May raise ValueError for invalid value
    '''

    name = attr['AttributeName']

    try:
        if attr['ReadOnly']:
            raise ValueError(f'BIOS attribute {name} is read-only')
    except KeyError:
        pass

    try:
        if attr['Immutable']:
            raise ValueError(f'BIOS attribute {name} is immutable')
    except KeyError:
        pass

    attr_type = attr['Type']
    if attr_type == 'String':
        return make_bios_attribute_string(attr, attr_value)

    if attr_type == 'Integer':
        return make_bios_attribute_integer(attr, attr_value)

    if attr_type == 'Boolean':
        return make_bios_attribute_boolean(attr, attr_value)

    if attr_type == 'Password':
        return make_bios_attribute_password(attr, attr_value)

    if attr_type == 'Enumeration':
        return make_bios_attribute_enum(attr, attr_value)

    raise NotImplementedError(f'setting BIOS attribute type {attr_type!r} is not implemented')


def make_bios_attribute_string(attr: Dict[str, Any], attr_value: Optional[str]) -> Optional[str]:
    '''Validates attr_value as BIOS attribute string value
    May raise ValueError
    '''

    if attr_value is None:
        # NOTE we accept `None` (or a JSON `null`) as a valid "String" value
        # at least I have seen Dell "Password" values being `null`
        return None

    name = attr['AttributeName']

    try:
        min_length = attr['MinLength']
        if len(attr_value) < min_length:
            raise ValueError(f'BIOS attribute {name} has minimum length {min_length}')
    except KeyError:
        pass

    try:
        max_length = attr['MaxLength']
        if len(attr_value) > max_length:
            raise ValueError(f'BIOS attribute {name} has maximum length {max_length}')
    except KeyError:
        pass

    try:
        value_expr = attr['ValueExpression']
        if isinstance(value_expr, str) and value_expr:
            m = re.match(value_expr, attr_value)
            if m is None:
                raise ValueError(f'BIOS attribute {name} must match regex {value_expr!r}')
    except KeyError:
        pass

    return attr_value


def make_bios_attribute_integer(attr: Dict[str, Any], attr_value: Optional[str]) -> int:
    '''Validates attr_value as BIOS attribute integer value
    May raise ValueError
    '''

    name = attr['AttributeName']

    if attr_value is None:
        raise ValueError(f'BIOS attribute {name} requires an integer value')

    try:
        ivalue = int(attr_value)
    except ValueError as err:
        raise ValueError(f'BIOS attribute {name} requires an integer value') from err

    try:
        lowerbound = attr['LowerBound']
        if ivalue < lowerbound:
            raise ValueError(f'BIOS attribute {name} must be >= {lowerbound}')
    except KeyError:
        pass

    try:
        upperbound = attr['UpperBound']
        if ivalue > upperbound:
            raise ValueError(f'BIOS attribute {name} must be <= {upperbound}')
    except KeyError:
        pass

    try:
        scalar_increment = attr['ScalarIncrement']
        if scalar_increment:
            if ivalue % scalar_increment != 0:
                raise ValueError(f'BIOS attribute {name} has scalar increment; must be a multiple of {scalar_increment}')
    except KeyError:
        pass

    return ivalue


def make_bios_attribute_boolean(attr: Dict[str, Any], attr_value: Optional[str]) -> bool:
    '''Validates attr_value as BIOS attribute boolean value
    May raise ValueError
    '''

    name = attr['AttributeName']

    if attr_value is None:
        raise ValueError(f'BIOS attribute {name} must be True or False')

    if attr_value.lower() == 'true':
        bvalue = True
    elif attr_value.lower() == 'false':
        bvalue = False
    else:
        raise ValueError(f'BIOS attribute {name} must be True or False')

    return bvalue


def make_bios_attribute_password(attr: Dict[str, Any], attr_value: Optional[str]) -> Optional[str]:
    '''Validates attr_value as BIOS attribute password value
    May raise ValueError
    '''

    # same checks as for type String
    return make_bios_attribute_string(attr, attr_value)


def make_bios_attribute_enum(attr: Dict[str, Any], attr_value: Optional[str]) -> str:
    '''Validates attr_value as BIOS attribute enumerated value
    May raise ValueError
    '''

    name = attr['AttributeName']

    # enums are strings, chosen from a limited set of literals

    valid_enums = {}                    # type: Dict[str, str]
    try:
        for enum_dict in attr['Value']:
            # index by lowercase of the enum value, so that casing does not matter
            enum_value = enum_dict['ValueName']
            valid_enums[enum_value.lower()] = enum_value
    except KeyError:
        # strange ... enum type without any values
        debug('warning: no valid enumeration values found')

    if attr_value is None:
        valid_msg = ', '.join(list(valid_enums.values()))
        raise ValueError(f'BIOS attribute {name} must be one of: {valid_msg}')

    try:
        return valid_enums[attr_value.lower()]

    except KeyError as err:
        valid_msg = ', '.join(list(valid_enums.values()))
        raise ValueError(f'BIOS attribute {name} must be one of: {valid_msg}') from err


def import_bios_attributes(filename: str, host: str, args: argparse.Namespace) -> None:
    '''import BIOS settings from file
    The file should be in JSON format

    May raise OSError, ValueError, MultiLineError, RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()

    with open(filename, encoding='utf-8') as fh:
        try:
            settings = json.load(fh)
        except json.decoder.JSONDecodeError as err:
            short_filename = os.path.basename(filename)
            raise ValueError(f'{short_filename}: {err}') from err

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            set_bios_attributes(redfish_obj, settings)
        except MultiLineError as err:
            # prepend the filename
            short_filename = os.path.basename(filename)
            err.messages = [short_filename + ': ' + x for x in err.messages]
            raise err


def set_bios_attributes(redfish_obj: RedfishClient, settings: Dict[str, Any]) -> None:
    '''patch the BIOS attributes
    This function also checks the settings against the registry

    May raise RedfishError, ValueError if invalid settings
    '''

    registry_list = obtain_bios_attribute_registry(redfish_obj)

    # note: this may raise a multi-line exception
    new_settings = check_bios_settings(settings, registry_list)

    # set the new settings
    settings_url = get_bios_settings_url(redfish_obj)
    body = {'Attributes': new_settings}
    headers = {'If-Match': '*',
               'Content-Type': 'application/json'}
    redfish_obj.patch(settings_url, body, headers)

    if restart_required(settings, registry_list):
        print('note: system restart required')


def check_bios_settings(settings: Dict[str, Any], registry_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''check given BIOS settings for validity against registry
    `settings` are case-insensitive; returned new settings will contain
    the 'official' case-sensitive attribute names

    Returns new dictionary with settings

    May raise MultiLineError with a multi-line error message
    '''

    errors = []

    new_settings = {}
    for key, value in settings.items():
        found = False
        for registry_item in registry_list:
            if registry_item['AttributeName'].lower() == key.lower():
                found = True
                try:
                    new_settings[registry_item['AttributeName']] = make_bios_attribute_value(registry_item, value)
                except ValueError as err:
                    errors.append(str(err))
                break
        if not found:
            errors.append(f'no such BIOS setting {key!r}')

    if errors:
        raise MultiLineError('there were errors', errors)

    return new_settings


def restart_required(settings: Dict[str, Any], registry_list: List[Dict[str, Any]]) -> bool:
    '''Returns True if changing one of the settings requires a system restart'''

    for key in settings.keys():
        for registry_item in registry_list:
            if registry_item['AttributeName'].lower() == key.lower() and registry_item['ResetRequired']:
                return True

    return False


def factoryreset_bios(host: str, args: argparse.Namespace) -> None:
    '''perform factoryreset of system BIOS using Redfish

    May raise RedfishError
    May raise KeyError if no system BIOS present
    '''

    # in theory there can be multiple members containing a BIOS in a system
    # we reset them all
    did_reset = 0

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        print('resetting system BIOS to factory defaults')

        urls = redfish_obj.query_urls('Systems:Members:Bios:Actions:#Bios.ResetBios:target')
        for url in urls:
            reset_response = redfish_obj.post(url, body={})

            # print messages (if any)
            msg = RedfishClient.get_message(reset_response).rstrip()
            if msg:
                print(msg)

            did_reset += 1

    if not did_reset:
        raise KeyError('system BIOS not found')


# EOB
