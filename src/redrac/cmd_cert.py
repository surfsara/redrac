#
#   redrac cmd_cert.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: cert

- shows certificates that are present in the remote controller
- generate CSR (interactive or by CSR config file)
- import a certificate (either pubcert or combined private + public in a single file)
'''

import os
import sys
import argparse

from typing import List, Dict, Optional, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.hostconf import get_host, ConfigErrors
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import get_vendor, is_vendor
from redrac.util import print_dict, print_data
from redrac.print import print                                      # pylint: disable=redefined-builtin


def do_cert(host: str, args: argparse.Namespace) -> None:
    '''handle the cert command

    cert [--csr [-f cnf_file] [--example]] [--import certfile]
    '''

    if args.file or args.example:
        # this is a fix for what's maybe an unclear user-interface;
        # the command `cert -f file.cnf` or `cert -x` should really have `--csr`
        args.csr = True

    num = 0
    if args.csr:
        num += 1
    if args.import_file:
        num += 1
    if args.import_bundle:
        num += 1
    if args.import_key_idrac:
        num += 1
    if num > 1:
        raise ValueError("options '--csr', '--import', '--import-bundle', '--import-key-idrac' can not be combined")

    if args.csr:
        generate_csr(host, args)

    elif args.import_file:
        import_certificate(host, args)

    elif args.import_bundle:
        import_bundle(host, args)

    elif args.import_key_idrac:
        idrac_import_key(host, args)

    else:
        show_certificate(host, args)


def show_certificate(host: str, args: argparse.Namespace) -> None:
    '''show server certificate of the remote controller'''

    data_list = load_certificates(host, args)

    if args.json:
        print_data(data_list, as_json=True)

    elif len(data_list) > 1:
        for idx, data in enumerate(data_list):
            cert_data = redfish_util.strip_redfish_keys(data)
            print(f'Certificate #{idx + 1}:')
            print_dict(cert_data)
            print()
    else:
        cert_data = redfish_util.strip_redfish_keys(data_list[0])
        print_dict(cert_data)


def load_certificates(host: str, args: argparse.Namespace) -> List[Dict[str, Any]]:
    '''Returns list of dictionaries containing certificate data
    May raise RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        urls = redfish_obj.query_urls('CertificateService:CertificateLocations:Links:Certificates')

        # the URL for the server certificate of the remote controller
        # typically starts with the manager_url
        # Heuristic: Therefore if we have a manager_url in there, then it's probably
        # the server certificate that we are looking for
        try:
            manager_url = redfish_obj.query_urls('Managers')[0]

            for cert_url in urls:
                if cert_url.startswith(manager_url):
                    return [redfish_obj.get(cert_url), ]

        except (KeyError, IndexError):
            pass

        # the manager's certificate was not found;
        # return any certificates that we find
        data_list = []
        for cert_url in urls:
            data_list.append(redfish_obj.get(cert_url))
        return data_list


def generate_csr(host: str, args: argparse.Namespace) -> None:
    '''generate certificate signing request'''

    if args.example:
        generate_csr_example_file(args.file, host)
        return

    if not args.file:
        params = get_interactive_csr_params(host)
    else:
        params = load_csr_config(args.file)
    debug(f'params == {params!r}')

    # double check (better safe than sorry)
    validate_csr_params(params)

    host, username, password, cafile = get_host(host, args).as_tuple()
    redfish_generate_csr_file(host, username, password, cafile, params, args.file)


def generate_csr_example_file(filename: str, host: str) -> None:
    '''write a 'template' file that contains an example CSR config for host'''

    if not filename:
        filename = 'csr_' + host.replace('.', '_') + '.cnf'

    short_host = host.split('.')[0]
    short_filename = os.path.basename(filename)
    short_cmd = os.path.basename(sys.argv[0])

    # exclusive create file;
    # this operation fails with OSError if the file already exists
    with open(filename, 'x', encoding='utf-8') as fh:
        fh.write(f'''#
#   {short_filename}
#
#   This config file creates a CSR with
#    {short_cmd} csr -f {short_filename} {host}
#

# the fully qualified domain name of the remote controller or management module
CommonName = {host}

# additional hostnames of the remote controller or management module
# put multiple hostnames on one line, separated by spaces
AlternativeNames =

# any additional information that identifies this subject
UnstructuredName = {short_host}

# name of the organization making the certificate signing request
Organization = the company name
# name of the unit or division or department within the organization
OrganizationalUnit =
# city or locality of the organization
City =
# state, province, or region
State =
# country (ISO Alpha-2 two letter code)
Country =
# e-mail address of the contact within the organization making the request
Email =

# name of the person making the request
Surname = your last name
GivenName = your first name
Initials = your initials
ContactPerson = your name

# KeyUsage describes the purpose of the public key in the certificate
# It may have the following values, separated by spaces on a single line:
#
# CRLSigning            DigitalSignature      KeyEncipherment
# ClientAuthentication  EmailProtection       NonRepudiation
# CodeSigning           EncipherOnly          OCSPSigning
# DataEncipherment      KeyAgreement          ServerAuthentication
# DecipherOnly          KeyCertSign           Timestamping
#
KeyUsage = ServerAuthentication

# password for revocation requests
ChallengePassword =
# select specific algorithm
# the algorithm must be one of TPM_ALG_IDs from
# the Trusted Computing Group Algorithm Registry
# Recommended values are TPM_ALG_RSA or TPM_ALG_ECDSA
# or leave it empty
KeyPairAlgorithm =
# length of the key, if needed by the KeyPairAlgorithm
KeyBitLength =
# curve id to be used by the key, if needed by the KeyPairAlgorithm
# Recommended values are:
#  TPM_ECC_NIST_P256 or TPM_ECC_NIST_P384 or TPM_ECC_NIST_P512
# or leave it empty
KeyCurveId =
# Redfish link to where the certificate will be installed
# e.g. /redfish/v1/Managers/BMC/NetworkProtocol/HTTPS/Certificates
# This will be automatically detected if left empty
CertificateCollection =

''')
    print(f'writing {filename}')


def print_list_of_enums(valid_enums: List[str]) -> None:
    '''show list of valid values to the user
    They are printed in columns
    '''

    max_width = 78
    max_elem_width = max(len(x) for x in valid_enums)
    num_columns = max_width // max_elem_width
    num_rows = (len(valid_enums) + num_columns - 1) // num_columns

    # make list of lists that represent columns and rows
    table = [['' for _i in range(num_columns)] for _j in range(num_rows)]
    i = 0
    j = 0
    for v in sorted(valid_enums):
        table[i][j] = v.ljust(max_elem_width)
        i += 1
        if i >= num_rows:
            i = 0
            j += 1

    # print the columns
    for row in table:
        line = '  '.join(row).strip()
        print(' ' + line)


def input_with_default(prompt: str, default_str: str) -> str:
    '''get interactive user input
    Entering an empty line returns the default
    Entering a space returns an empty string
    '''

    if default_str:
        if prompt.endswith(': '):
            prompt = prompt[:-2]
        prompt += f' [{default_str}]: '

    line = input(prompt)
    if not line:
        return default_str
    return line.strip()


def get_interactive_csr_params(host: str) -> Dict[str, Any]:
    '''get user input for CSR parameters
    Asks user for confirmation that the parameters look correct
    and allows user to retry entering all the information

    Returns parameters as dictionary
    May raise ValueError
    '''

    while True:
        params = enter_csr_params(host)
        print()
        print()
        print_csr_params(params)
        print()

        while True:
            yesno = input('Does this look correct? (Yes/No): ').lower()
            if yesno in ('n', 'no'):
                print()
                break

            if yesno in ('y', 'yes'):
                return params


def print_csr_params(params: Dict[str, Any]) -> None:
    '''show the entered CSR parameters'''

    for key, value in params.items():
        if isinstance(value, list):
            svalue = ' '.join([str(x) for x in value])
            print(f'{key}: {svalue}')
        else:
            print(f'{key}: {value}')


def enter_csr_params(host: str) -> Dict[str, Any]:
    '''get user input for CSR parameters
    Returns parameters as dictionary
    May raise ValueError
    '''

    params = {}                         # type: Dict[str, Any]

    if '.' in host:
        params['CommonName'] = strip_quotes(input_with_default('CommonName (remote controller FQDN): ', host))
    else:
        params['CommonName'] = strip_quotes(input('CommonName (remote controller FQDN): ').strip())
    validate_csr_commonname(params['CommonName'])

    alt_names = strip_quotes(input('AlternativeNames (separated by spaces): ').strip())
    params['AlternativeNames'] = alt_names.split()

    short_name = host.split('.')[0]
    params['UnstructuredName'] = strip_quotes(input_with_default('UnstructuredName: ', short_name))

    params['Organization'] = strip_quotes(input('Organization: ').strip())
    params['OrganizationalUnit'] = strip_quotes(input('OrganizationalUnit (department): ').strip())
    params['City'] = strip_quotes(input('City: ').strip())
    params['State'] = strip_quotes(input('State or province: ').strip())
    validate_csr_state(params['State'])
    params['Country'] = strip_quotes(input('Country (two letter code): ').strip())
    validate_csr_country(params['Country'])
    params['Email'] = strip_quotes(input('Email of organization contact person: ').strip())

    params['Surname'] = strip_quotes(input('Surname (your last name): '))
    params['GivenName'] = strip_quotes(input('GivenName (your first name): '))
    params['Initials'] = strip_quotes(input('Initials (of your name): '))
    contact_person = (params['GivenName'] + ' ' + params['Surname']).strip()
    params['ContactPerson'] = strip_quotes(input_with_default('ContactPerson (your name): ', contact_person))

    print('''
KeyUsage defines the purpose of the public key in the certificate.
It may have the following values, or just leave it empty:
''')
    print_list_of_enums(CSR_KEY_USAGE)
    print()
    while True:
        try:
            key_usage = strip_quotes(input('KeyUsage (separated by spaces): ').strip())
            key_usage_list = key_usage.split()
            validate_csr_key_usage(key_usage_list)
            params['KeyUsage'] = key_usage_list
            break
        except ValueError as err:
            print(f'error: {err}', file=sys.stderr)
            # retry input
            continue

    params['ChallengePassword'] = strip_quotes(input('ChallengePassword (for revocation requests): '))

    print('''
Recommended values for KeyPairAlgorithm are: TPM_ALG_RSA  TPM_ALG_ECDSA
or just leave it empty.
''')
    params['KeyPairAlgorithm'] = strip_quotes(input('KeyPairAlgorithm (TPM_ALG algorithm): ').strip())
    validate_csr_keypairalgorithm(params['KeyPairAlgorithm'])
    keybitlength = strip_quotes(input('KeyBitLength (if required): ').strip())
    if keybitlength:
        try:
            params['KeyBitLength'] = int(keybitlength)
            validate_csr_keybitlength(params['KeyBitLength'])
        except ValueError as err:
            raise ValueError('invalid KeyBitLength') from err

    print('''
Recommended values for KeyCurveId are:
  TPM_ECC_NIST_P256  TPM_ECC_NIST_P384  TPM_ECC_NIST_P512
or just leave it empty.
''')
    params['KeyCurveId'] = strip_quotes(input('KeyCurveId (for ECC algorithms): ').strip())
    validate_csr_keycurveid(params['KeyCurveId'])

    params['CertificateCollection'] = strip_quotes(input('CertificateCollection URL: ').strip())

    # filter out empty values
    params = {key: value for key, value in params.items() if value}

    return params


def strip_quotes(svalue: str) -> str:
    '''Removes single or double quotes around string value
    Returns the stripped string
    '''

    # strip any quotes
    if svalue.startswith('"') and svalue.endswith('"'):
        return svalue.strip('"')
    if svalue.startswith("'") and svalue.endswith("'"):
        return svalue.strip("'")
    return svalue


def csr_make_post_body(params: Dict[str, Any]) -> Dict[str, Any]:
    '''make body dictionary for POST request to generate a CSR
    Returns the dictionary
    '''

    # for the most part, body equals params
    body = params.copy()
    try:
        body['CertificateCollection'] = {'@odata.id': params['CertificateCollection']}
    except KeyError:
        pass
    return body


def csr_make_post_body_dell(params: Dict[str, Any]) -> Dict[str, Any]:
    '''make body dictionary for POST request to generate a CSR
    specifically for a Dell/iDRAC system

    Dell does not accept all keys in the CSR body
    even though they are documented in Redfish

    Returns the dictionary
    '''

    dell_params = params.copy()
    # delete these keys from the parameters
    for key in ('ContactPerson', 'Surname', 'GivenName', 'Initials', 'KeyUsage'):
        try:
            del dell_params[key]
        except KeyError:
            continue
    return csr_make_post_body(dell_params)


def find_cert_collection(redfish_obj: RedfishClient, cert_locations: str, manager_url: str) -> str:
    '''Returns CertificateCollection URL
    This is the path component of server certificate URL

    cert_locations is the URL to CertificateService/CertificateLocations/
    manager_url is the URL to the Redfish Manager section
    '''

    cert_loc_response = redfish_obj.get(cert_locations)

    # the URL for the server certificate of the remote controller
    # typically starts with the manager_url
    # Heuristic: Therefore if we have a manager_url in there, then it's probably
    # the server certificate that we are looking for
    for cert_location in cert_loc_response['Links']['Certificates']:
        cert_url = cert_location['@odata.id']
        if cert_url.startswith(manager_url):
            collection = os.path.dirname(cert_url)
            debug(f'CertificateCollection == {collection}')
            # return early
            return collection

    # the manager's certificate was not found;
    # return path of first certificate that we find
    for cert_location in cert_loc_response['Links']['Certificates']:
        cert_url = cert_location['@odata.id']
        collection = os.path.dirname(cert_url)
        debug(f'CertificateCollection == {collection}')
        # return early
        return collection

    # not found
    debug('CertificateCollection not found (returning empty string)')
    return ''


def redfish_generate_csr_file(host: str, username: str, password: str, cafile: str, params: Dict[str, Any], filename: Optional[str] = '') -> None:
    '''generate CSR by doing a Redfish POST request
    params is a dictionary containing the CSR parameters
    The generated CSR will be written to filename or {host}.csr

    May raise RedfishError, OSError
    '''

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        if 'CertificateCollection' not in params:
            try:
                manager_url = redfish_obj.query_urls('Managers')[0]
                debug(f'manager_url == {manager_url}')
            except (KeyError, IndexError) as err:
                raise RedfishError('failed to get Redfish manager URL') from err

            try:
                cert_locations = redfish_obj.query_urls('CertificateService:CertificateLocations')[0]
                debug(f'cert_locations == {cert_locations}')
            except (KeyError, IndexError) as err:
                raise RedfishError('failed to get Redfish certificate locations URL') from err

            collection = find_cert_collection(redfish_obj, cert_locations, manager_url)
            if collection:
                params['CertificateCollection'] = collection

        try:
            csr_url = redfish_obj.query_urls('CertificateService:Actions:#CertificateService.GenerateCSR:target')[0]
        except (KeyError, IndexError) as err:
            raise RedfishError('failed to get Redfish CSR URL') from err

        if is_vendor(get_vendor(redfish_obj), 'DELL'):
            body = csr_make_post_body_dell(params)
        else:
            body = csr_make_post_body(params)
        csr_response = redfish_obj.post(csr_url, body)
        csr_string = csr_response['CSRString']

    if not filename:
        filename = host.replace('.', '_') + '.csr'
    else:
        filename = replace_filename_ext(filename, '.csr')
    write_csr(csr_string, filename)


def replace_filename_ext(filename: str, ext: str) -> str:
    '''Returns filename with given extension'''

    name, _old_ext = os.path.splitext(filename)
    return name + ext


def write_csr(csr_string: str, filename: str) -> None:
    '''write CSR string to file
    May raise OSError
    '''

    with open(filename, 'w', encoding='utf-8') as fh:
        fh.write(csr_string)
    print(f'writing {filename}')


def load_csr_config(filename: str) -> Dict[str, Any]:
    '''load CSR config file
    Returns dictionary with CSR parameters
    May raise OSError, ValueError

    This function will exit the program if there are any errors
    in the config file
    '''

    params = {}                         # type: Dict[str, Any]

    known_csr_params = ['CommonName', 'AlternativeNames', 'Organization', 'OrganizationalUnit',
                        'City', 'State', 'Country', 'Email',
                        'KeyUsage', 'ChallengePassword', 'UnstructuredName',
                        'Surname', 'GivenName', 'Initials', 'ContactPerson',
                        'KeyPairAlgorithm', 'KeyBitLength', 'KeyCurveId', 'CertificateCollection']
    lower_csr_params = {x.lower(): x for x in known_csr_params}

    validate_table = {'CommonName': validate_csr_commonname,
                      'State': validate_csr_state,
                      'Country': validate_csr_country,
                      'KeyUsage': validate_csr_key_usage,
                      'KeyBitLength': validate_csr_keybitlength,
                      'KeyCurveId': validate_csr_keycurveid,
                      'KeyPairAlgorithm': validate_csr_keypairalgorithm}

    debug(f'loading {filename}')
    with open(filename, encoding='utf-8') as fh:
        lines = fh.readlines()

    errors = []

    for idx, line in enumerate(lines):
        lineno = idx + 1

        line = line.strip()
        if not line:
            continue

        if line[0] == '#':
            # skip comment
            continue

        try:
            key, value = line.split('=', maxsplit=1)
            key = key.strip()
            value = strip_quotes(value.strip())
        except ValueError:
            errors.append((filename, lineno, "expected 'key=value'"))
            continue

        try:
            param = lower_csr_params[key.lower()]
            params[param] = value
        except KeyError:
            errors.append((filename, lineno, f'invalid CSR parameter: {key!r}'))
            continue

        # some parameters are not of type string ...
        if param == 'AlternativeNames':
            params['AlternativeNames'] = params['AlternativeNames'].split()

        elif param == 'KeyUsage':
            params['KeyUsage'] = params['KeyUsage'].split()

        elif param == 'KeyBitLength':
            try:
                if params['KeyBitLength']:
                    params['KeyBitLength'] = int(params['KeyBitLength'])
            except ValueError:
                errors.append((filename, lineno, 'invalid KeyBitLength'))
                continue

        # validate the parameter
        try:
            # get validation function from table
            validation_func = validate_table[param]
        except KeyError:
            pass
        else:
            try:
                # invoke the validation function
                validation_func(params[param])                  # type: ignore
            except ValueError as err:
                errors.append((filename, lineno, str(err)))
                continue

    # filter out empty values
    params = {key: value for key, value in params.items() if value}

    # if CommonName was not present, then it was never validated either
    # but it is always required
    if 'CommonName' not in params:
        errors.insert(0, (filename, 0, 'missing CSR parameter: CommonName'))

    if errors:
        raise ConfigErrors('there were CSR config errors', errors)

    return params


def validate_csr_params(params: Dict[str, Any]) -> None:
    '''Raises ValueError if invalid CSR parameters are found'''

    try:
        validate_csr_commonname(params['CommonName'])
    except KeyError as err:
        raise ValueError('CommonName is required') from err

    try:
        validate_csr_state(params['State'])
    except KeyError:
        pass

    try:
        validate_csr_country(params['Country'])
    except KeyError:
        pass

    try:
        validate_csr_key_usage(params['KeyUsage'])
    except KeyError:
        pass

    try:
        validate_csr_keypairalgorithm(params['KeyPairAlgorithm'])
    except KeyError:
        pass

    try:
        validate_csr_keybitlength(params['KeyBitLength'])
    except KeyError:
        pass

    try:
        validate_csr_keycurveid(params['KeyCurveId'])
    except KeyError:
        pass


def validate_csr_commonname(commonname: str) -> None:
    '''Raises ValueError if invalid CommonName'''

    if not commonname:
        raise ValueError('the CommonName is required')

    if '*' in commonname:
        raise ValueError('Redfish GenerateCSR is not suitable for wildcard certificates. Use `openssl req` instead')


def validate_csr_state(state: str) -> None:
    '''Raises ValueError if invalid State'''

    if not state:
        raise ValueError('the State is required')


def validate_csr_country(country: str) -> None:
    '''Raises ValueError if invalid Country'''

    if len(country) != 2:
        raise ValueError('Country must be a two letter code')


CSR_KEY_USAGE = ['ClientAuthentication', 'CodeSigning', 'CRLSigning', 'DataEncipherment',
                 'DecipherOnly', 'DigitalSignature', 'EmailProtection', 'EncipherOnly',
                 'KeyAgreement', 'KeyCertSign', 'KeyEncipherment', 'NonRepudiation',
                 'OCSPSigning', 'ServerAuthentication', 'Timestamping']


def validate_csr_key_usage(key_usage_list: List[str]) -> None:
    '''Raises ValueError if invalid keyUsage enum string is found'''

    for key_usage in key_usage_list:
        if key_usage not in CSR_KEY_USAGE:
            raise ValueError(f'invalid KeyUsage value: {key_usage!r}')


def validate_csr_keypairalgorithm(algorithm: str) -> None:
    '''Raises ValueError if invalid KeyPairAlgorithm'''

    if algorithm and not algorithm.startswith('TPM_ALG_'):
        raise ValueError(f'invalid KeyPairAlgorithm: {algorithm!r}')


def validate_csr_keybitlength(keybitlength: int) -> None:
    '''Raises ValueError if invalid KeyBitLength'''

    try:
        assert keybitlength > 0
    except (ValueError, AssertionError) as err:
        raise ValueError('invalid KeyBitLength') from err


def validate_csr_keycurveid(keycurveid) -> None:
    '''Raises ValueError if invalid KeyCurveId'''

    if keycurveid and not keycurveid.startswith('TPM_ECC_'):
        raise ValueError(f'invalid KeyCurveId: {keycurveid!r}')


def load_certificate_file(filename: str) -> str:
    '''Load certificate file
    Returns the certificate as string
    May raise OSError, ValueError if invalid file
    '''

    debug(f'loading {filename}')
    with open(filename, encoding='utf-8') as fh:
        cert_string = fh.read().rstrip()

    if '--BEGIN CERTIFICATE--' not in cert_string or '--END CERTIFICATE--' not in cert_string:
        raise ValueError(f'{filename} does not contain a certificate in PEM format')

    # the certificate file may contain a private key as well
    # this is not mandatory, but acceptable
    if '--BEGIN PRIVATE KEY--' in cert_string:
        if '--END PRIVATE KEY--' not in cert_string:
            raise ValueError(f'{filename} is corrupted or contains incomplete data')
        if cert_string.count('--BEGIN PRIVATE KEY--') > 1:
            raise ValueError(f'bundle {filename} contains multiple private keys')
        debug(f'{filename} contains both certificate and private key')

    return cert_string


def import_certificate(host: str, args: argparse.Namespace) -> None:
    '''import new server certificate
    May raise ValueError, RedfishError
    '''

    cert_string = load_certificate_file(args.import_file)

    host, username, password, cafile = get_host(host, args).as_tuple()
    redfish_import_certificate_string(host, username, password, cafile, cert_string)
    print("ok")


def redfish_import_certificate_string(host: str, username: str, password: str, cafile: str, cert_string: str) -> None:
    '''import certificate by replacing the current manager certificate
    May raise RedfishError
    '''

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        manager_cert_url = ''

        urls = redfish_obj.query_urls('CertificateService:CertificateLocations:Links:Certificates')

        # the URL for the server certificate of the remote controller
        # typically starts with the manager_url
        # Heuristic: Therefore if we have a manager_url in there, then it's probably
        # the server certificate that we are looking for
        try:
            manager_url = redfish_obj.query_urls('Managers')[0]

            for cert_url in urls:
                if cert_url.startswith(manager_url):
                    manager_cert_url = cert_url
                    break

        except (KeyError, IndexError):
            pass

        if not manager_cert_url:
            raise RedfishError('failed to determine manager certificate URL')

        try:
            replace_url = redfish_obj.query_urls('CertificateService:Actions:#CertificateService.ReplaceCertificate:target')[0]
        except (KeyError, IndexError) as err:
            raise RedfishError('failed to determine Redfish replace-certificate URL') from err

        body = {'CertificateUri': {'@odata.id': manager_cert_url},
                'CertificateString': cert_string,
                'CertificateType': 'PEM'}
        redfish_obj.post(replace_url, body)


def load_cert_bundle_file(filename: str) -> str:
    '''Load certificate file, to be placed into a bundle
    Returns the content as string
    May raise OSError, ValueError if invalid file
    '''

    debug(f'loading {filename}')
    with open(filename, encoding='utf-8') as fh:
        content = fh.read().rstrip()

    if '--BEGIN CERTIFICATE--' in content and '--END CERTIFICATE--' in content:
        return content

    if '--BEGIN PRIVATE KEY--' in content and '--END PRIVATE KEY--' in content:
        return content

    if '--BEGIN PRIVATE KEY--' in content or '--END PRIVATE KEY--' in content:
        raise ValueError(f'{filename} contains incomplete PEM data')

    if '--BEGIN CERTIFICATE--' in content or '--END CERTIFICATE--' in content:
        raise ValueError(f'{filename} contains incomplete PEM data')

    raise ValueError(f'{filename} does not contain a certificate in PEM format')


def load_cert_bundle_files(files: List[str]) -> str:
    '''load multiple PEM files
    Returns file contents as a single string
    May raise ValueError, OSError
    '''

    contents = []
    for filename in files:
        content = load_cert_bundle_file(filename)
        contents.append(content)
    return '\n'.join(contents)


def import_bundle(host: str, args: argparse.Namespace) -> None:
    '''import a certificate bundle
    May raise ValueError, OSError, RedfishError
    '''

    if '+' in args.import_bundle:
        cert_string = load_cert_bundle_files(args.import_bundle.split('+'))
    else:
        cert_string = load_certificate_file(args.import_bundle)

    if cert_string.count('--BEGIN PRIVATE KEY--') > 1:
        raise ValueError('bundle contains multiple private keys')

    host, username, password, cafile = get_host(host, args).as_tuple()
    redfish_import_certificate_string(host, username, password, cafile, cert_string)
    print("ok")


def load_private_key_file(filename: str) -> str:
    '''Load private key file
    Returns the private key as string
    May raise OSError, ValueError if invalid file
    '''

    debug(f'loading {filename}')
    with open(filename, encoding='utf-8') as fh:
        privkey_string = fh.read().rstrip()

    if '--BEGIN PRIVATE KEY--' not in privkey_string or '--END PRIVATE KEY--' not in privkey_string:
        raise ValueError(f'{filename} does not contain a private key in PEM format')

    if '--BEGIN CERTIFICATE--' in privkey_string and '--END CERTIFICATE--' in privkey_string:
        raise ValueError(f'{filename} contains a certificate in PEM format')

    if  '--BEGIN CERTIFICATE--' in privkey_string or '--END CERTIFICATE--' in privkey_string:
        raise ValueError(f'{filename} contains incomplete or corrupted PEM data')

    return privkey_string


def idrac_import_key(host: str, args: argparse.Namespace) -> None:
    '''import private key into iDRAC
    This is a vendor-specific function for Dell iDRAC

    May raise ValueError, OSError, RedfishError
    '''

    privkey_string = load_private_key_file(args.import_key_idrac)

    host, username, password, cafile = get_host(host, args).as_tuple()
    idrac_upload_ssl_key(host, username, password, cafile, privkey_string)
    print("ok")


def idrac_upload_ssl_key(host: str, username: str, password: str, cafile: str, sslkey_string: str) -> None:
    '''upload SSL key into iDRAC
    May raise RedfishError
    '''

    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        try:
            upload_url = redfish_obj.query_urls('Managers:Members:Links:Oem:Dell:DelliDRACCardService:Actions:#DelliDRACCardService.UploadSSLKey:target')[0]
        except (KeyError, IndexError):
            raise RedfishError(f'iDRAC card service UploadSSLKey not found; is {host} an iDRAC?')

        body = {'SSLKeyString': sslkey_string}
        redfish_obj.post(upload_url, body)

# EOB
