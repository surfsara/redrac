#
#   redrac main.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''`redrac` is a utility for sysadmins for doing out-of-band management
on systems. It uses the Redfish API to interface with remote management
controllers. Redfish is a standard, and in principle any device
(whether be it a computer system, storage box, or network appliance)
that implements Redfish, can be addressed with `redrac`.
'''

import sys
assert sys.version_info >= (3, 6)

import argparse                                                         # noqa: E402
import redfish                                                          # noqa: E402

import redrac.debug                                                     # noqa: E402

from redrac.debug import debug                                          # noqa: E402
from redrac.redfishcode import RedfishError, InvalidJSONError           # noqa: E402
from redrac.hostconf import ConfigErrors                                # noqa: E402
from redrac.util import MultiLineError                                  # noqa: E402
from redrac.cmd_cfg import do_cfg                                       # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_log import do_log                                       # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_hwinfo import do_hwinfo                                 # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_fwinfo import do_fwinfo                                 # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_fwupdate import do_fwupdate                             # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_biosattr import do_biosattr                             # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_boot import do_boot                                     # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_network import do_network                               # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_cert import do_cert                                     # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_passwd import do_passwd                                 # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_led import do_led                                       # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_thermals import do_thermals                             # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_fans import do_fans                                     # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_psu import do_psu                                       # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_power import do_power                                   # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_racreset import do_racreset                             # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_path import do_path                                     # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_url import do_url                                       # pylint: disable=unused-import # noqa: F401,E402
from redrac.cmd_load import do_load                                     # pylint: disable=unused-import # noqa: F401,E402
from redrac.print import print                                          # pylint: disable=redefined-builtin,disable=unused-import # noqa: F401,E402


def do_command(command: str, host: str, args: argparse.Namespace) -> None:
    '''perform remote controller command'''

    # get function by name
    this_module = sys.modules[__name__]
    try:
        func = getattr(this_module, 'do_' + command)
    except AttributeError:
        print(f"error: unknown command: '{command}'")
        sys.exit(1)

    # call the command function
    # first argument is always the target host
    func(host, args)


def get_options() -> argparse.Namespace:
    '''parse command-line options
    Returns argparse.Namespace
    '''

    parser = argparse.ArgumentParser('redrac')
    subparsers = parser.add_subparsers(dest='command')

    p_log = subparsers.add_parser('log', help='Get system log')
    p_log.add_argument('-j', '--json', action='store_true', help='Output in JSON format')
    p_log.add_argument('-f', '--clear', '--flush', action='store_true', help='Clear all logs')
    p_log.add_argument('--sel', action='store_true', help='Show Dell SEL (system event log)')
    p_log.add_argument('--lc', action='store_true', help='Show Dell LcLog (lifecycle controller log)')

    p_cfg = subparsers.add_parser('cfg', help='Get system configuration / change hostname')
    p_cfg.add_argument('-j', '--json', action='store_true', help='Output in JSON format')
    p_cfg.add_argument('--hostname', metavar='HOSTNAME', default='', help='Change hostname property')
    p_cfg.add_argument('-V', '--vendor', action='store_true', help='Show vendor string (for: match-vendor, assert-vendor)')

    p_hwinfo = subparsers.add_parser('hwinfo', help='Get hardware information')
    p_hwinfo.add_argument('-j', '--json', action='store_true', help='Output in JSON format')

    p_fwinfo = subparsers.add_parser('fwinfo', help='Get firmware versions')
    p_fwinfo.add_argument('-j', '--json', action='store_true', help='Output in JSON format')

    p_fwupdate = subparsers.add_parser('fwupdate', help='Install firmware update')
    p_fwupdate.add_argument('-f', '--file', metavar='FILE', default='', help='Firmware filename')
    p_fwupdate.add_argument('-t', '--target', metavar='TARGET', default='', help='Firmware target (default: none)')
    p_fwupdate.add_argument('--clearlock', action='store_true', help="Force clear any firmware update locks ('busy' status)")

    p_biosattr = subparsers.add_parser('biosattr', help='Manage BIOS settings')
    p_biosattr.add_argument('-j', '--json', action='store_true', help='Show BIOS attributes in JSON format')
    p_biosattr.add_argument('-a', '--attr', metavar='ATTR', help='Show specific BIOS attribute')
    p_biosattr.add_argument('-s', '--set', metavar='ATTR=VALUE', help='Change BIOS setting')
    p_biosattr.add_argument('-x', '--export', action='store_true', help='Export BIOS settings (JSON format)')
    p_biosattr.add_argument('-i', '--import', dest='import_file', metavar='FILE', help='Import BIOS settings from file')
    p_biosattr.add_argument('--factory-reset', action='store_true', help='Reset BIOS settings to factory defaults')

    p_boot = subparsers.add_parser('boot', help='Manage boot order')
    p_boot.add_argument('-j', '--json', action='store_true', help='Show boot order in JSON format')
    p_boot.add_argument('-s', '--set', metavar='ORDER', help='Change boot order (comma-separated list)')

    p_network = subparsers.add_parser('network', help='Manage network settings')
    p_network.add_argument('-j', '--json', action='store_true', help='Show network settings in JSON format')
    p_network.add_argument('-x', '--export', action='store_true', help='Export network settings (JSON format)')
    p_network.add_argument('-i', '--import', dest='import_file', metavar='FILE', help='Import network settings from file')

    p_cert = subparsers.add_parser('cert', help='Manage server certificate')
    p_cert.add_argument('-j', '--json', action='store_true', help='Show certificate in JSON format')
    p_cert.add_argument('--csr', action='store_true', help='Generate CSR (certificate signing request)')
    p_cert.add_argument('-f', '--file', metavar='CONFIG', default='', help='Use CSR config file')
    p_cert.add_argument('-x', '--example', action='store_true', help='Generate example CSR config file')
    p_cert.add_argument('-i', '--import', dest='import_file', metavar='CERT', help='Import a certificate')
    p_cert.add_argument('--import-bundle', metavar='CERT+KEY', help='Import a certificate bundle')
    p_cert.add_argument('--import-key-idrac', metavar='KEY', help='Import a private key into iDRAC')

    p_passwd = subparsers.add_parser('passwd', help='Change root/admin password')
    p_passwd.add_argument('-f', '--file', metavar='FILE', required=False, default='', help='Read new password from file')
    p_passwd.add_argument('--old-password', metavar='PASSWORD', required=False, default='', help='Log in with old password; new password in config file')

    p_led = subparsers.add_parser('led', help='Show or control indicator LED')
    p_led.add_argument('-j', '--json', action='store_true', help='Show in JSON format')
    p_led.add_argument('--on', action='store_true', help='Turn on indicator LED')
    p_led.add_argument('--off', action='store_true', help='Turn off indicator LED')

    p_thermals = subparsers.add_parser('thermals', help='Control temperature readings and thresholds')
    p_thermals.add_argument('-j', '--json', action='store_true', help='Show in JSON format')
    p_thermals.add_argument('-n', '--name', metavar='SENSOR', help='Sensor name')
    p_thermals.add_argument('-t', '--threshold', action='store_true', help='Change thermal thresholds')
    p_thermals.add_argument('-W', '--warn', '--warning', '--caution', metavar='LOWER:UPPER', help='Set lower:upper caution threshold')
    p_thermals.add_argument('-C', '--crit', '--critical', metavar='LOWER:UPPER', help='Set lower:upper critical threshold')

    p_fans = subparsers.add_parser('fans', help='Show fan status')
    p_fans.add_argument('-j', '--json', action='store_true', help='Output in JSON format')

    p_psu = subparsers.add_parser('psu', help='Show PSU status')
    p_psu.add_argument('-j', '--json', action='store_true', help='Output in JSON format')

    p_power = subparsers.add_parser('power', help='Control power')
    p_power.add_argument('-l', '--list', action='store_true', help='Show available power actions')
    p_power.add_argument('-f', '--force', metavar='STATE', help='Apply new power state')

    p_racreset = subparsers.add_parser('racreset', help='Restart of remote controller')
    p_racreset.add_argument('-l', '--list', action='store_true', help='Show available reset types')
    p_racreset.add_argument('-f', '--force', metavar='STATE', help='Restart the remote controller')
    p_racreset.add_argument('--factory-reset', action='store_true', help='Reset remote controller to factory defaults')

    p_path = subparsers.add_parser('path', help='Access Redfish object via pseudo-path')
    p_path.add_argument('path', metavar='PATH')
    p_path.add_argument('-m', '--match', metavar='KEY=PATTERN', help='Select target URL by matching property')
    p_path.add_argument('-u', '--url-match', metavar='PATTERN', help='Select target URL by matching URL string')
    p_path.add_argument('-j', '--json', action='store_true', help='Output in JSON format')
    p_path.add_argument('--get', action='store_true', help='Send HTTP GET request (default)')
    p_path.add_argument('--post', action='store_true', help='Send HTTP POST request')
    p_path.add_argument('--patch', action='store_true', help='Send HTTP PATCH request')
    p_path.add_argument('--delete', action='store_true', help='Send HTTP DELETE request')
    p_path.add_argument('-i', '--import', dest='import_file', metavar='FILE', help='Import Redfish settings from JSON file')
    p_path.add_argument('--body', dest='body_file', metavar='FILE', help='Send HTTP request body from JSON file')
    p_path.add_argument('--headers', dest='headers_file', metavar='FILE', help='Send HTTP request headers from JSON file')
    p_path.add_argument('-x', '--export', action='store_true', help='Export Redfish object (JSON format)')
    p_path.add_argument('-t', '--template', action='store_true', help='Export Redfish object as redrac JSON command')
    p_path.add_argument('-a', '--attrib', action='append', help='Export only specific Redfish object attributes')
    p_path.add_argument('-w', '--wait', action='store_true', help='Wait for asynchronous task to complete')

    p_url = subparsers.add_parser('url', help='Access Redfish object via URL')
    p_url.add_argument('url', metavar='URL')
    p_url.add_argument('-j', '--json', action='store_true', help='Output in JSON format')
    p_url.add_argument('--get', action='store_true', help='Send HTTP GET request (default)')
    p_url.add_argument('--post', action='store_true', help='Send HTTP POST request')
    p_url.add_argument('--patch', action='store_true', help='Send HTTP PATCH request')
    p_url.add_argument('--delete', action='store_true', help='Send HTTP DELETE request')
    p_url.add_argument('-i', '--import', dest='import_file', metavar='FILE', help='Import Redfish settings from JSON file')
    p_url.add_argument('--body', dest='body_file', metavar='FILE', help='Send HTTP request body from JSON file')
    p_url.add_argument('--headers', dest='headers_file', metavar='FILE', help='Send HTTP request headers from JSON file')
    p_url.add_argument('-x', '--export', action='store_true', help='Export Redfish object (JSON format)')
    p_url.add_argument('-t', '--template', action='store_true', help='Export Redfish object as redrac JSON command')
    p_url.add_argument('-a', '--attrib', action='append', help='Export only specific Redfish object attributes')
    p_url.add_argument('-w', '--wait', action='store_true', help='Wait for asynchronous task to complete')

    p_load = subparsers.add_parser('load', help='Control Redfish objects via redrac JSON file(s)')
    p_load.add_argument('files', metavar='FILE', nargs='+')
    p_load.add_argument('-v', '--assign', metavar='VAR=VALUE', action='append', help='Assign value to $VAR')

    parser.add_argument('-c', '--config', metavar='CONFIG', default='', help='Specify alternate config file')
    # NOTE default username='root' is coded into get_host() ... for a reason
    parser.add_argument('-u', '--username', metavar='USER', default='', help='Specify username (default: root)')
    parser.add_argument('-p', '--askpassword', action='store_true', help='Ask for password')
    parser.add_argument('-P', '--password', metavar='PASSWD', default='', help='Pass password on command-line (insecure)')
    parser.add_argument('--cafile', metavar='FILE', default='', help='Pass CA bundle file for SSL connection')
    parser.add_argument('-D', '--debug', action='store_true', help='Show debug messages')
    parser.add_argument('--version', action='version', version=f'''%(prog)s {redrac.__version__} (c) 2023 SURF bv''', help='Show version number and exit')
    parser.add_argument('host', help='Remote controller hostname or IP address')

    args = parser.parse_args()

    if args.debug:
        redrac.debug.DEBUG = True
        debug(f'args == {args!r}')

    return args


def run():
    '''main program, really'''

    opts_ = get_options()
    if opts_.debug:
        # run without catching (most) exceptions
        try:
            do_command(opts_.command, opts_.host, opts_)
        except BrokenPipeError:
            sys.exit(128 + 13)

    else:
        try:
            do_command(opts_.command, opts_.host, opts_)

        except ConfigErrors as err_:
            print(f'{err_}', file=sys.stderr)
            sys.exit(255)

        except MultiLineError as err_:
            for msg in err_.messages:
                print(f'error: {msg}', file=sys.stderr)
            sys.exit(255)

        except KeyError as err_:
            # print KeyError without double quotes (Python quirk)
            msg_ = f'{err_}'.strip('"')
            print(f'error: {msg_}', file=sys.stderr)
            sys.exit(255)

        except (ValueError, NotImplementedError, RedfishError) as err_:
            print(f'error: {err_}', file=sys.stderr)
            sys.exit(255)

        except KeyboardInterrupt:
            sys.exit(128 + 2)

        except BrokenPipeError:
            sys.exit(128 + 13)

        except OSError as err_:
            if err_.filename is not None:
                print(f'error: {err_.filename}: {err_.strerror}', file=sys.stderr)
            else:
                print(f'error: {err_.strerror}', file=sys.stderr)
            sys.exit(255)

        except InvalidJSONError:
            print('error: invalid JSON in response')
            sys.exit(255)

        # redfish exceptions may have empty message ...
        # so we print the message by ourselves
        except redfish.rest.v1.RetriesExhaustedError:
            print('error: connection failed; retries exhausted', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.InvalidCredentialsError:
            print('error: connection failed; invalid credentials', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.SessionCreationError:
            print('error: connection failed; session creation error', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.ServerDownOrUnreachableError:
            print('error: connection failed; server unreachable', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.DecompressResponseError:
            print('error: connection failed; error in decompressing response', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.JsonDecodingError:
            print('error: connection failed; invalid JSON in response', file=sys.stderr)
            sys.exit(255)

        except redfish.rest.v1.BadRequestError:
            print('error: connection failed; bad request', file=sys.stderr)
            sys.exit(255)



if __name__ == '__main__':
    run()


# EOB
