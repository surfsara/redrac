#
#   redrac debug.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''debug function'''

import os
import inspect

from typing import List


PROG = 'redrac'
DEBUG = False
DEBUG_COLORS = os.isatty(1)


def debug(msg: str) -> None:
    '''print debug message (if in debug mode)'''

    if not DEBUG:
        return

    stack = inspect.stack()
    frame_info = stack[1]
    try:
        classname = frame_info.frame.f_locals['self'].__class__.__name__
    except KeyError:
        # no 'self' defined
        classname = ''

    for line in msg.splitlines(keepends=False):
        output: List[str] = []
        if DEBUG_COLORS:
            output.append('\x1b[32m')

        if classname:
            output.append(f'% {PROG} {classname}.{frame_info.function}(): ')
        else:
            output.append(f'% {PROG} {frame_info.function}(): ')

        if DEBUG_COLORS:
            output.append('\x1b[0m')

        output.append(line)
        print(''.join(output))

# EOB
