#
#   redrac progress.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''progress ticker'''

import sys
import time
import threading

from redrac import debug


class Progress:
    '''a progress ticker that prints a series of dots while busy'''

    SLEEP_TIME = 0.333  # seconds
    MAX_TICKS = 10

    def __init__(self) -> None:
        '''initialize instance'''

        self.ticks = 0
        self.printed = False
        self.thread = threading.Thread(target=self.run_as_thread, daemon=True)
        self.stopping = False

    def busy(self) -> bool:
        '''Returns True if busy running as thread'''

        return self.thread.is_alive()

    def start(self) -> None:
        '''start running as thread'''

        if debug.DEBUG or not sys.stdout.isatty():
            # do not start; no output will be produced
            return

        if not self.thread.is_alive():
            self.stopping = False
            self.thread.start()

    def stop(self) -> None:
        '''stop running thread'''

        if self.thread.is_alive():
            self.stopping = True
            self.thread.join()
            self.stopping = False
            self.clear()
            # threads can only be started once
            # so we simply create a new thread right now
            self.thread = threading.Thread(target=self.run_as_thread, daemon=True)

    def run_as_thread(self) -> None:
        '''print progress ticks'''

        while not self.stopping:
            time.sleep(Progress.SLEEP_TIME)
            if self.stopping:
                break

            self.tick()

    def tick(self) -> None:
        '''output one tick'''

        self.ticks += 1
        if self.ticks > Progress.MAX_TICKS:
            self.ticks = 0
            sys.stdout.write('\r')
        else:
            sys.stdout.write('.')
            self.printed = True
        sys.stdout.flush()

    def clear(self) -> None:
        '''clear output'''

        if self.printed:
            spaces = ' ' * (Progress.MAX_TICKS + 1)
            sys.stdout.write(f'\r{spaces}\r')
            sys.stdout.flush()
            self.printed = False
            self.ticks = 0



# global progress ticker
TICKER = Progress()


def start() -> None:
    '''start the global progress ticker'''

    TICKER.start()


def stop() -> None:
    '''stop the global progress ticker'''

    TICKER.stop()


# EOB
