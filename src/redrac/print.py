#
#   redrac print.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''print output

We redefine the print() function only to control the looks
of the console output when a progress ticker is busy
'''

import builtins

from redrac import progress


def print(*args, **kwargs) -> None:                                 # pylint: disable=redefined-builtin
    '''print output'''

    # stop progress ticker (if any)
    progress.stop()

    # call original print() function
    builtins.print(*args, **kwargs)


# EOB
