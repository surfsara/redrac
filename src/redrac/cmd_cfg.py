#
#   redrac cmd_cfg.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements the command: cfg

Shows information about the remote management controller and the system
'''

import argparse
import json

from typing import List, Dict, Any

from redrac import progress
from redrac import redfish_util
from redrac.debug import debug
from redrac.hostconf import get_host
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.redfish_util import get_vendor
from redrac.util import print_dict, print_data
from redrac.print import print


def short_hostname(hostname: str) -> str:
    '''Returns short hostname, without any domain'''

    try:
        return hostname.split('.')[0]
    except (ValueError, IndexError) as err:
        raise ValueError('invalid hostname') from err


def do_cfg(host: str, args: argparse.Namespace) -> None:
    '''get system configuration'''

    if args.hostname:
        change_hostname(short_hostname(args.hostname), host, args)
        return

    if args.vendor:
        cfg_get_vendor(host, args)
        return

    data_list = redfish_util.load('Systems:Members', host, args)

    if args.json:
        print_data(data_list, as_json=True)
        return

    cfg = cfg_from_data(data_list)
    print_dict(cfg)


def cfg_get_vendor(host: str, args: argparse.Namespace) -> None:
    '''show vendor string'''

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        vendor = get_vendor(redfish_obj)

    if args.json:
        data = {'vendor': vendor}
        print_data(data, as_json=args.json)
    else:
        print(f'vendor: {vendor!r}')


def cfg_from_data(data_list: List[Dict[str, Any]]) -> Dict[str, Any]:
    '''filter Redfish data, we only wish to see certain keys
    Returns new cfg dictionary with system properties
    '''

    properties = ['HostName', 'Model', 'Manufacturer', 'SystemType',
                  'SerialNumber', 'AssetTag', 'ServiceTag', 'SKU', 'PartNumber', 'UUID',
                  'BiosVersion',
                  'PowerState', 'Status',
                  'ProcessorSummary', 'MemorySummary', 'TrustedModules']

    cfg = {}

    for member in data_list:
        for key in properties:
            try:
                if key not in cfg:
                    cfg[key] = member[key]
                    debug(f'cfg[{key!r}] == {cfg[key]!r}')
            except KeyError:
                pass

    cfg = redfish_util.strip_redfish_keys(cfg)
    return cfg


def find_hostname_url(redfish_obj: RedfishClient, path: str) -> str:
    '''Returns URL to the HostName property
    May raise RedfishError, or KeyError if not found
    '''

    urls = redfish_obj.query_urls(path)
    debug(f'path == {path!r}  urls == {json.dumps(urls, indent=2)}')

    for url in urls:
        data = redfish_obj.get(url)
        if 'HostName' in data:
            return url

    raise KeyError('URL to HostName not found')


def change_hostname(hostname: str, host: str, args: argparse.Namespace) -> None:
    '''change the Systems:HostName property
    May raise RedfishError
    '''

    # there are two HostNames;
    # one is the computer system's hostname,
    # one is the management controller's hostname
    # we set both to the same short name

    host, username, password, cafile = get_host(host, args).as_tuple()
    progress.start()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        # change the Systems HostName
        try:
            hostname_url = find_hostname_url(redfish_obj, 'Systems:Members')
            debug(f'hostname_url == {hostname_url}')
        except KeyError as err:
            raise RedfishError('Redfish URL to Systems HostName not found') from err

        body = {'HostName': hostname}
        headers = {'If-Match': '*',
                   'Content-Type': 'application/json'}
        redfish_obj.patch(hostname_url, body, headers)

        # change the Managers HostName
        try:
            hostname_url = find_hostname_url(redfish_obj, 'Managers:Members:EthernetInterfaces:Members')
            debug(f'hostname_url == {hostname_url}')
        except KeyError as err:
            raise RedfishError('Redfish URL to the Managers HostName not found') from err

        redfish_obj.patch(hostname_url, body, headers)

    print('ok')


# EOB
