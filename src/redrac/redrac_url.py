#
#   redrac redrac_url.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''access direct redfish URL from the command-line'''

import sys
import argparse
import json

from typing import Dict, Any

import redrac.debug
from redrac.redfishcode import RedfishClient, RedfishError
from redrac.hostconf import get_host
from redrac.util import print_dict


def format_json(d: Dict[str, Any]) -> str:
    '''Returns pretty-printed JSON string'''

    return json.dumps(d, indent=2)


def get_url(url: str, host: str, args: argparse.Namespace) -> Dict[str, Any]:
    '''get data from remote controller using redfish
    Returns dictionary
    May raise RedfishError
    '''

    host, username, password, cafile = get_host(host, args).as_tuple()
    with RedfishClient.login(host, username, password, cafile) as redfish_obj:
        response = redfish_obj.get(url)
        return response


def get_options() -> argparse.Namespace:
    '''parse command-line options
    Returns argparse.Namespace
    '''

    parser = argparse.ArgumentParser('redrac-url')
    parser.add_argument('-c', '--config', metavar='CONFIG', default='', help='Specify alternate config file')
    # NOTE default username='root' is coded into get_host() ... for a reason
    parser.add_argument('-u', '--username', metavar='USER', default='', help='Specify username (default: root)')
    parser.add_argument('-p', '--password', action='store_true', help='Ask for password')
    parser.add_argument('--cafile', metavar='FILE', default='', help='Pass CA bundle file for SSL connection')
    parser.add_argument('--json', action='store_true', help='Output in JSON format')
    parser.add_argument('-D', '--debug', action='store_true', help='Show debug messages')
    parser.add_argument('host', help='Remote controller hostname or IP address')
    parser.add_argument('URL', help='redfish URL')

    args = parser.parse_args()

    if args.debug:
        redrac.debug.PROG = 'redrac-url'
        redrac.debug.DEBUG = True

    return args


def run():
    '''main program, really'''

    opts_ = get_options()
    try:
        data_ = get_url(opts_.URL, opts_.host, opts_)

        if opts_.json:
            print('{}'.format(format_json(data_)))
        else:
            print_dict(data_)

    except (ValueError, RedfishError) as err_:
        print(f'error: {err_}', file=sys.stderr)
        sys.exit(255)



if __name__ == '__main__':
    run()


# EOB
