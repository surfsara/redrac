#
#   redrac thresholds.py
#
#   Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

'''implements thresholds'''

from typing import Dict, Optional, Any


class Thresholds:
    '''represents sensor thresholds'''

    def __init__(self) -> None:
        '''initialize instance'''

        self.lower_caution: Optional[float] = None
        self.upper_caution: Optional[float] = None
        self.lower_caution_user: Optional[float] = None
        self.upper_caution_user: Optional[float] = None
        self.lower_critical: Optional[float] = None
        self.upper_critical: Optional[float] = None
        self.lower_critical_user: Optional[float] = None
        self.upper_critical_user: Optional[float] = None
        self.lower_fatal: Optional[float] = None
        self.upper_fatal: Optional[float] = None

    @staticmethod
    def header() -> str:
        '''Returns header line for printing columns of thresholds'''

        warn = 'warning'
        crit = 'critical'
        return f'{warn:^11}        {crit:^11}'

    def __repr__(self) -> str:
        '''Returns string'''

        return (f'Thresholds(lower_caution={self.lower_caution!r}, upper_caution={self.upper_caution!r}, '
                f'lower_caution_user={self.lower_caution_user!r}, upper_caution_user={self.upper_caution_user!r}, '
                f'lower_critical={self.lower_critical!r}, upper_critical={self.upper_critical!r}, '
                f'lower_critical_user={self.lower_critical_user!r}, upper_critical_user={self.upper_critical_user!r}, '
                f'lower_fatal={self.lower_fatal!r}, upper_fatal={self.upper_fatal!r})')

    def __str__(self) -> str:
        '''Returns string showing the relevant thresholds'''

        # we make four columns:
        # lower caution|upper caution    lower critical|upper critical
        #
        # the user threshold overrides the 'default' threshold
        # we do not show the fatal threshold, unless it goes over the given threshold

        # caution/warning
        lower_warn = None                   # type: Optional[float]
        if self.lower_caution_user is not None:
            lower_warn = self.lower_caution_user
        elif self.lower_caution is not None:
            lower_warn = self.lower_caution
        else:
            lower_warn = self.lower_fatal
        if lower_warn is not None and self.lower_fatal is not None and lower_warn < self.lower_fatal:
            lower_warn = self.lower_fatal
        if lower_warn is not None:
            s_lower_warn = f'{lower_warn:>5.1f}'
        else:
            s_lower_warn = '-'

        upper_warn = None                   # type: Optional[float]
        if self.upper_caution_user is not None:
            upper_warn = self.upper_caution_user
        elif self.upper_caution is not None:
            upper_warn = self.upper_caution
        else:
            upper_warn = self.upper_fatal
        if upper_warn is not None and self.upper_fatal is not None and upper_warn > self.upper_fatal:
            upper_warn = self.upper_fatal
        if upper_warn is not None:
            s_upper_warn = f'{upper_warn:>5.1f}'
        else:
            s_upper_warn = '-'

        # critical
        lower_crit = None                   # type: Optional[float]
        if self.lower_critical_user is not None:
            lower_crit = self.lower_critical_user
        elif self.lower_critical is not None:
            lower_crit = self.lower_critical
        else:
            lower_crit = self.lower_fatal
        if lower_crit is not None and self.lower_fatal is not None and lower_crit < self.lower_fatal:
            lower_crit = self.lower_fatal
        if lower_crit is not None:
            s_lower_crit = f'{lower_crit:>5.1f}'
        else:
            s_lower_crit = '-'

        upper_crit = None                   # type: Optional[float]
        if self.upper_critical_user is not None:
            upper_crit = self.upper_critical_user
        elif self.upper_critical is not None:
            upper_crit = self.upper_critical
        else:
            upper_crit = self.upper_fatal
        if upper_crit is not None and self.upper_fatal is not None and upper_crit > self.upper_fatal:
            upper_crit = self.upper_fatal
        if upper_crit is not None:
            s_upper_crit = f'{upper_crit:>5.1f}'
        else:
            s_upper_crit = '-'

        return f'{s_lower_warn:>5} {s_upper_warn:>5}        {s_lower_crit:>5} {s_upper_crit:>5}'

    @staticmethod
    def from_data(data: Dict[str, Any]) -> 'Thresholds':
        '''instantiate from data dictionary'''

        t = Thresholds()

        try:
            t.lower_caution = float(data['LowerCaution']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.lower_caution_user = float(data['LowerCautionUser']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.upper_caution = float(data['UpperCaution']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.upper_caution_user = float(data['UpperCautionUser']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.lower_critical = float(data['LowerCritical']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.lower_critical_user = float(data['LowerCriticalUser']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.upper_critical = float(data['UpperCritical']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.upper_critical_user = float(data['UpperCriticalUser']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.lower_fatal = float(data['LowerFatal']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass
        try:
            t.upper_fatal = float(data['UpperFatal']['Reading'])
        except (KeyError, ValueError, TypeError):
            pass

        return t

    def as_dict(self) -> Dict[str, Any]:
        '''Returns instance as dictionary'''

        return {'LowerCaution': self.lower_caution,
                'UpperCaution': self.upper_caution,
                'LowerCautionUser': self.lower_caution_user,
                'UpperCautionUser': self.upper_caution_user,
                'LowerCritical': self.lower_critical,
                'UpperCritical': self.upper_critical,
                'LowerCriticalUser': self.lower_critical_user,
                'UpperCriticalUser': self.upper_critical_user,
                'LowerFatal': self.lower_fatal,
                'UpperFatal': self.upper_fatal}


# EOB
