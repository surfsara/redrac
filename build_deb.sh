#! /bin/bash
#
#	build_deb.sh
#

if [[ -z $1 ]]
then
	echo "usage: build_deb.sh <release-tag>"
	exit 1
fi

if [[ ! -e src/redrac/main.py ]]
then
	echo "error: please chdir to the source directory"
	exit 1
fi

CI_COMMIT_TAG=$1

TAG_VERSION=$(echo $CI_COMMIT_TAG | awk -F- '{ print $2 }')
TAG_RELEASE=$(echo $CI_COMMIT_TAG | awk -F- '{ if ($3) { print $3 } else { print 1 } }')

# debian build system grabs the version number from the changelog
# update the changelog if this version number is not there
# (which is a hacky fix, but we're bound to forget to update it anyway)
if ! grep -q "${TAG_VERSION}-$TAG_RELEASE" debian/changelog
then
	DEBEMAIL="info@surf.nl"
	export DEBEMAIL
	dch -v "${TAG_VERSION}-$TAG_RELEASE" --distribution stable --package redrac "changes unknown; built from commit tag"
fi

fakeroot debian/rules clean
PATH=$(pwd)/debian:$PATH fakeroot debian/rules binary
fakeroot debian/rules clean

# EOB
