#! /bin/bash
#
#	build_rpm.sh
#

if [[ -z $1 ]]
then
	echo "usage: build_rpm.sh <release-tag>"
	exit 1
fi

if [[ ! -e src/redrac/cmd_cfg.py ]]
then
	echo "error: please chdir to the toplevel source directory"
	exit 1
fi

TAG=$1

VERSION=$(echo $TAG | awk -F- '{ print $2 }')
RELEASE=$(echo $TAG | awk -F- '{ if ($3) { print $3 } else { print 1 } }')
rpmbuild -ba redrac.spec \
	--define "tag_version $VERSION" \
	--define "tag_release $RELEASE" \
	--define "_topdir $(pwd)" \
	--define "_sourcedir $(pwd)"

# EOB
