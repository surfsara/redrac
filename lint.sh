#! /bin/bash

if [ ! -z "$GITLAB_CI" ]
then
	set -x -e
fi

# use 'ruff' if we have it
if type ruff >/dev/null 2>&1
then
	if [ -z "$1" ]
	then
		ruff check .
	else
		ruff check "$@"
	fi
	ret=$?
	if [ $ret -eq 0 ]
	then
		echo "Success: no errors found"
	fi
	exit $ret
else
	# use pylint/mypy/flake8
	errs=0
	if [ -z "$1" ]
	then
		for f in src/redrac/*.py
		do
			echo "+ $f"
			pylint "$f" || errs=$((errs + 1))
			mypy "$f" || errs=$((errs + 1))
			flake8 "$f" || errs=$((errs + 1))
		done
	else
		pylint "$@" || errs=$((errs + 1))
		mypy "$@" || errs=$((errs + 1))
		flake8 "$@" || errs=$((errs + 1))
	fi
	if [ $errs -gt 255 ]
	then
		errs=255
	fi
	if [ $errs -ne 0 ]
	then
		echo "there were errors"
	fi
	exit $errs
fi

# EOB
