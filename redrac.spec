#
#	redrac.spec
#	Copyright 2023 SURF bv
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#

Summary: redrac out-of-band management utility
Name: redrac
Version: %{tag_version}
Release: %{tag_release}
License: ASL 2.0
Group: Applications/System
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: %{_arch}
Vendor: SURF <helpdesk@surf.nl>
Url: https://gitlab.com/surfsara/redrac/
AutoReqProv: no
BuildRequires: python39, rpm-build, git

%description
redrac is a utility for sysadmins for doing out-of-band management
on systems. It uses the Redfish API to interface with remote management
controllers.

%prep
###%setup -n redrac-%{version}

%build
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
rm -rf %{buildroot}

%install
cd %{_sourcedir}
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
mkdir -p %{buildroot}/opt/venvs/redrac %{buildroot}%{_bindir}
python3.9 -m venv %{buildroot}/opt/venvs/redrac
source %{buildroot}/opt/venvs/redrac/bin/activate
pip install --upgrade pip
pip install -r %{_sourcedir}/requirements.txt
pip install flit venvctrl
flit build
FLIT_ROOT_INSTALL=1 flit install
pip uninstall -y flit
venvctrl-relocate --source %{buildroot}/opt/venvs/redrac --destination /opt/venvs/redrac
deactivate
# delete annoying scripts with wrong python interpreter shebangs
rm -rf %{buildroot}/opt/venvs/redrac/lib/python*/site-packages/requests_unixsocket/tests
%{__install} -D -m 0755 -o root -g root %{_sourcedir}/debian/redrac-wrapper.sh %{buildroot}%{_bindir}/redrac
%{__install} -D -m 0644 -o root -g root %{_sourcedir}/LICENSE.txt %{buildroot}%{_datadir}/doc/redrac/LICENSE.txt
%{__install} -D -m 0644 -o root -g root %{_sourcedir}/README.md %{buildroot}%{_datadir}/doc/redrac/README.md
%{__install} -D -m 0644 -o root -g root %{_sourcedir}/data/examples/redrac.conf.example %{buildroot}%{_datadir}/doc/redrac/redrac.conf.example
%{__install} -D -m 0644 -o root -g root %{_sourcedir}/data/examples/csr_serv-a1_mgmt_surf_nl.cnf.example %{buildroot}%{_datadir}/doc/redrac/csr_serv-a1_mgmt_surf_nl.cnf.example
%{__install} -D -m 0600 -o root -g root %{_sourcedir}/data/examples/redrac.conf.example %{buildroot}%{_sysconfdir}/redrac.conf

%clean
rm -f %{_sourcedir}/INSTALLED_FILES %{_sourcedir}/MANIFEST
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/redrac
/opt/venvs/redrac
%{_datadir}/doc/redrac
%config(noreplace) %{_sysconfdir}/redrac.conf

%changelog
